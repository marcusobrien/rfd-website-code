//Includes required to use Roboclaw library
#include <SoftwareSerial.h>
#include "RoboClaw.h"
#include <Wire.h>
#include "I2CMyNetwork.h"

//TO DOWNLOAD THIS PROGRAM MAKE SURE THE BLACK SWITCH ON THE BT IS SET TO OFF 
//WHEN USING THE bt IE DRIVING THE UNIT - TURN THE BT SWITCH TO on

String SWVersion("V1.5.0");

#define DEBUG
//#define MDM

//2 X Problems
//1. too fast when turning - steer 
//2. Stops too far away when going fast
//4. Fix up the comms so we wait until BT is ready first

//Use this to print out the object distances coming in via i2c
#define OUTPUT_STATS

//Bluetooth													Arduino

//YOU CAN NOT TRANSMIT FROM THE SERIAL MONITOR IF THE BT IS ON ! sO DONT SEND ANYTHING FROM THERE

//Level shift - Transmit on Arduinioo to Receive on BT
//Transmit of th Arduino is brown then goes to black goes to the 1k side of the level shift
// the centre of the level shift (yellow then brown) the brown goes to receive of the bluetooth)
//The 2k side of the level shift goes to the gnd orange from arduino - orange on level shift

//The receive of the Arduino orange goes to grey on the switch - other side of switch goes to 
//white then the white goes to the txd on the BT

//RoboClaw
//Connecting the RoboClaw
//Pin 5 arduino Blue cable - S1 RC
//Pin 4 arduino Yellow cable - S2 RC
//Green +
//Blue -


//TODO
// 3. When driving need to check distances - done forward and back
// 4. Need to be able to brake and slow - done change direction is done
// 5. Need to be able to turn dead, and live

//I2C
//SCL clock A5
//SDA Data A4

//Versions
//1. Initial
//2. Added i2c and receive object distances
//3. Addd validation for startup

//Tx should go to S1
//Rx should go to S2

//Receive pin 5 YELLOW - S2
//Transmit pin 6 BLUE - S1
SoftwareSerial RCSerial(5, 6);

//Using Packet Serial Mode - Address 0x80
RoboClaw roboclaw(&RCSerial, 10000);
#define SendStatsEveryXLoops 100

#define RCAddress 0x80
#define baudRateToRoboClaw 9600

#define LOOP_DELAY 50
#define FIRST_DEAD_TURN_SPEED 40
#define FIRST_SPEED 18

#define MAXIMUM_SPEED 75
#define SPEED_CHANGE_AMOUNT 5

//Amount to slow down one of the motors by
//when steering/turning
#define TURN_SPEED_SLOW_DOWN 4

//Speed for the robot as normal
int CurrentMALSpeed = 0;

//When turning - subtract this speed for the slow side
int CurrentMALTurningSpeed = 0;

//This is hte factor we mutiply the speed by to get the min safe distance
#define SPEED_DEPEND_DISTANCE_RAMP_UP 1.5

//This is the safe distance calculated with speed taken into account 
int safe_distance_account_for_speed = SAFE_OBJECT_DISTANCE;

#define MS_BLINK_DELAY 500

int LastResults[NUM_SONICS];

//Pin configs
#define BUMPER_L_PIN 7  
#define BUMPER_R_PIN 11

//If e Stop is hit - we dont know it - so no leds
//If bumper then front blue
//if IR then then middle blue
//If no floor then - flash both blues
//When re-starting we will use the orange LED

#define MIDDLE_BLUE_LED_PIN 8
#define BUMPER_BLUE_LED_PIN 9
#define ORANGE_LED_PIN 12 //RESTART LED

enum MALMode
{
	Kid = 0,
	Auto = 1,
	Manual = 2
} MALmode;

void resetLEDs()
{
	digitalWrite(BUMPER_BLUE_LED_PIN, LOW);
	digitalWrite(MIDDLE_BLUE_LED_PIN, LOW);
	digitalWrite(ORANGE_LED_PIN, LOW);
}

//To leave it on just pass in an odd number, the num includes on and off
void signal(int times, bool bumper_blue, bool mid_blue = false, bool orange = false)
{
	bool enable_blue = bumper_blue;
	bool enable_mid_blue = mid_blue;
	bool enable_orange = orange;

	for (int count = 0; count < times; count++)
	{
		if (bumper_blue)
		{
			if (enable_blue)
			{
				digitalWrite(BUMPER_BLUE_LED_PIN, HIGH);
				enable_blue = false;
			}
			else
			{
				digitalWrite(BUMPER_BLUE_LED_PIN, LOW);
				enable_blue = true;
			}
		}
		if (mid_blue)
		{
			if (enable_mid_blue)
			{
				digitalWrite(MIDDLE_BLUE_LED_PIN, HIGH);
				enable_mid_blue = false;
			}
			else
			{
				digitalWrite(MIDDLE_BLUE_LED_PIN, LOW);
				enable_mid_blue = true;
			}
		}
		if (orange)
		{
			if (enable_orange)
			{
				digitalWrite(ORANGE_LED_PIN, HIGH);
				enable_orange = false;
			}
			else
			{
				digitalWrite(ORANGE_LED_PIN, LOW);
				enable_orange = true;
			}
		}
		delay(MS_BLINK_DELAY);
	}
}

int getMin(int a, int b, int c = 0)
{
	int Min = safe_distance_account_for_speed + 1;

	if (a != 0)
	{
		Min = a;
	}
 	else if(b != 0)
	{
		Min = b;
	}
	else if (c != 0)
	{
		Min = c;
	}

	if (a != 0)
	{
		if (Min > a)
			Min = a;
	}
	if (b != 0)
	{
		if (Min > b)
			Min = b;
	}
	if (c != 0)
	{
		if (Min > c)
			Min = c;
	}
	return Min;
}

void workOutsafeDistance()
{
	safe_distance_account_for_speed = CurrentMALSpeed * SPEED_DEPEND_DISTANCE_RAMP_UP;
	if (safe_distance_account_for_speed < SAFE_OBJECT_DISTANCE)
		safe_distance_account_for_speed = SAFE_OBJECT_DISTANCE;

#ifdef DEBUG
	Serial.print("SF:");
	Serial.print(safe_distance_account_for_speed, DEC);
	Serial.print(" Sp:");
	Serial.println(CurrentMALSpeed);
#endif
}

bool canMoveBackward()
{
	//Make sure distances are not less than SAFE_OBJECT_DISTANCE
	//Nearest object in back
	int nearestBack = getMin(LastResults[5], LastResults[6]);

#ifdef DEBUG
	Serial.print("DReV:");
	Serial.println(nearestBack, DEC);
#endif

	if (nearestBack < safe_distance_account_for_speed)
	{
#ifdef DEBUG
		Serial.println("DR:X");
		return false;
#endif
	}
	return true;
}

bool canMoveForward()
{
	//Make sure distances are not less than SAFE_OBJECT_DISTANCE
	//Nearest object in front 
	int nearestFront = getMin(LastResults[1], LastResults[2], LastResults[8]);
#ifdef DEBUG
	Serial.print("D-F:");
	Serial.println(nearestFront, DEC);
#endif

	if (nearestFront < safe_distance_account_for_speed)
	{
#ifdef DEBUG
		Serial.println("DF:X");
#endif
		return false;
	}
	return true;
}

void checkSafeDistance()
{
#ifdef OUTPUT_STATS
	for (int i = 0; i < NUM_SONICS; i++)
	{
		Serial.print("US#");
		Serial.print(i, DEC);
		Serial.print(':');
		Serial.print(LastResults[i], DEC);
		Serial.println("cm");
	}
#endif

	if (MAL_CurrentState == MovingForward)
	{
		if (canMoveForward() == false)
		{
				reverse();
				//Stop();
		}
	}
	else if (MAL_CurrentState == MovingBackward)
	{
		if (canMoveBackward() == false)
		{
			forward();
			//Stop();
		}
	}
}

//After we get a start token down i2c we expect the sonic stats, so turn this on
//turn it off when we have finished, or get an end token, or a weird value in the comms
bool Expectingi2cDistanceStat = false;

#define i2cidcharIndex 3
#define i2cDistancecharIndexX100 5
#define i2cDistancecharIndexX10 6
#define i2cDistancecharIndexX1 7

//Parse the distances and populate our lat rading list
void handleI2CObjectDistances(unsigned int bytes)
{
	int indexUSS = -1;
	static const char *testUltraSonicData = "US:0=133";
  
	if (bytes != strlen(testUltraSonicData))
	{
#ifdef DEBUG
		Serial.println("i2-US");
#endif
#if MDM
		sendToMDM("Err-ODM-US len");
#endif
		Expectingi2cDistanceStat = false;
		return;
	}

	//Make sure first 2 chars are the data US tokne
	if (Wire.read() == 'U' && Wire.read() == 'S' && Wire.read() == ':')
	{
#ifdef OUTPUT_STATS
		Serial.println("i2c-Got token - US:");
#endif
	}
	else
	{
#ifdef DEBUG
		Serial.println("Err-ODM-no US: tkn");
#endif
#if MDM
		sendToMDM("Err-ODM-no US: tkn");
#endif
		Expectingi2cDistanceStat = false;
		return;
	}

	char newCharacter = Wire.read();
		
	//This should be the sensor Id
	indexUSS = newCharacter-'0';

#ifdef OUTPUT_STATS
	Serial.print("i2c-US:");
	Serial.print(indexUSS);
	Serial.println(":");
#endif
	
	if (indexUSS < 0 || indexUSS > NUM_SONICS - 1)
	{
#ifdef DEBUG
		Serial.println("Err-ODM-no snsr-id");
#endif
		//An error - we didnt get a valid sensor id for which sensor the data is for ie 0 to 8
#if MDM
		sendToMDM("Err-ODM-no snsr-id");
#endif    
		Expectingi2cDistanceStat = false;
		return;
	}

	if (Wire.read() != '=')
	{
#ifdef DEBUG
		Serial.println("Err-ODM-no =");
#endif
		//An error - we didnt get the = sign
#if MDM
		sendToMDM("Err-ODM-no =");
#endif
		Expectingi2cDistanceStat = false;
		return;
	}

	LastResults[indexUSS] = ((Wire.read() - '0') * 100);
	LastResults[indexUSS] += ((Wire.read()  - '0') * 10);
	LastResults[indexUSS] += Wire.read() - '0';
	
	//If this is the last index ie sensor 9 index 8 - thn dont look for any more data after this round
	if (indexUSS == NUM_SONICS - 1)
	{
#ifdef OUTPUT_STATS
		Serial.println("i2cUSinx");
#endif
		//This is the last sensors data , adter this we dont look for any more
		Expectingi2cDistanceStat = false;

	}

#ifdef OUTPUT_STATS
	Serial.print("iUS:");
	Serial.print(indexUSS);
	Serial.print("=");
	Serial.print(LastResults[indexUSS]);
	Serial.println("cm");
#endif
}

//Got an i2c event
void receiveEvent(int bytes)
{	
#ifdef OUTPUT_STATS
	Serial.println("i2c evt");
#endif 
	if (Expectingi2cDistanceStat)
	{
		handleI2CObjectDistances(bytes);
		return;
	}

	static const char *OBJECT_DATA_START_TOKEN = "START:";
	//static const char *OBJECT_DATA_END_TOKEN = "END:";

	if (bytes < 1)
	{
#ifdef DEBUG
    static const char *msg = "Ei2 0ln";
		Serial.println(msg);
#endif
#if MDM
		sendToMDM(msg);
#endif
		return;
	}

	String msg = "";

	for (int charNum = 0; charNum < bytes; charNum++)
	{
		char newCharacter = Wire.read();
		msg += newCharacter;

		//Correct length for the start token ?
		if (bytes == (int)strlen(OBJECT_DATA_START_TOKEN))
		{
#ifdef OUTPUT_STATS
			Serial.println("ic rec-sk");
#endif
			//Find out if its a start token
			if (newCharacter == OBJECT_DATA_START_TOKEN[0])
			{
				unsigned int index = 1;
				for (; index < strlen(OBJECT_DATA_START_TOKEN); ++index)
				{
					newCharacter = Wire.read();
					msg += newCharacter;
					if (newCharacter != OBJECT_DATA_START_TOKEN[index])
					{
#ifdef DEBUG
						Serial.println("ic rec-sk s-ER");
#endif
						break;
					}
				}
				if (index == strlen(OBJECT_DATA_START_TOKEN))
				{
#ifdef OUTPUT_STATS
					Serial.print("ic rec-STRT:");
					Serial.println(msg);
#endif
					Expectingi2cDistanceStat = true;
					return;
				}
				else
				{
#ifdef DEBUG
					Serial.println("ic rc-sk st f-l");
#endif
				}
			}
		}
		Expectingi2cDistanceStat = false;
	}
	
#ifdef OUTPUT_STATS
	Serial.print("I2C Data:");
	Serial.println(msg);
#endif
}

//Push the driving stats to the MDM
void sendStatsToMDM()
{
	sendToMDM(getStateString());
}

String getStateString()
{
	String speedMsg("Sp:");
	speedMsg += String(CurrentMALSpeed);
	speedMsg += " Mvg:";

	if (MAL_CurrentState == CurrentState::DeadTurningLeft)
		return speedMsg + "-DTL";
	else if (MAL_CurrentState == CurrentState::DeadTurningRight)
		return speedMsg + "-DTR";
	else if (MAL_CurrentState == CurrentState::EStopped)
		return speedMsg + "-Crshd";
	else if (MAL_CurrentState == CurrentState::MovingBackward)
		return speedMsg + "Re";
	else if (MAL_CurrentState == CurrentState::MovingForward)
		return speedMsg + "Fw";
	else if (MAL_CurrentState == CurrentState::Stopped)
		return speedMsg + "XX";
	else
		return speedMsg + "??";
}

void printStatus()
{
	Serial.println("Status");
	Serial.print(RCAddress);
	Serial.println(":RC:");
	Serial.print(baudRateToRoboClaw);
	Serial.println("bd->RC");
	Serial.print(LOOP_DELAY);
	Serial.println("s LDel");
	Serial.print(FIRST_DEAD_TURN_SPEED);
	Serial.println("s dTsp");
	Serial.print(FIRST_SPEED);
	Serial.println(" strt sp");
	Serial.print(MAXIMUM_SPEED);
	Serial.println(" Mx Sp");
	Serial.print(SPEED_CHANGE_AMOUNT);
	Serial.println(" sp +");
	Serial.print(TURN_SPEED_SLOW_DOWN);
	Serial.println(" tsp dr");
	Serial.print(CurrentMALSpeed);
	Serial.println(" Sp");
	Serial.print(CurrentMALTurningSpeed);
	Serial.println(" cr tn sp");
	Serial.print(SPEED_DEPEND_DISTANCE_RAMP_UP);
	Serial.println(" dst sp dp");
	Serial.print(safe_distance_account_for_speed);
	Serial.println(" c sf dst");
	Serial.print(MS_BLINK_DELAY);
	Serial.println(" ms blk dly");
	Serial.print(MALmode);
	Serial.println(" mde-k/a/n");
}

//When something happens, ie crash, restart etc
void changeState(CurrentState newState)
{
	switch (newState)
	{
		case CurrentState::EStopped:
			MAL_CurrentState = newState;
#ifdef DEBUG
			Serial.println("XCrsdX");
#endif
			CurrentMALSpeed = 0;
			break;
		case CurrentState::MovingBackward:
		case CurrentState::MovingForward:
			resetLEDs();
			MAL_CurrentState = newState;
#ifdef DEBUG
			Serial.println("Mvg");
#endif
			break;
		case CurrentState::Stopped:
			CurrentMALSpeed = 0;
			resetLEDs();
			MAL_CurrentState = newState;
#ifdef DEBUG
			Serial.println("X");
#endif
			break;
		case CurrentState::DeadTurningLeft:
		case CurrentState::DeadTurningRight:
			resetLEDs();
			MAL_CurrentState = newState;
#ifdef DEBUG
			Serial.println("DTrn");
#endif
			break;
		default:
			CurrentMALSpeed = 0;
#ifdef DEBUG
			Serial.println("U-St");
#endif
			break;
	}
}

//Set up everything
void setup()
{
	MALmode = MALMode::Manual;

	Serial.begin(9600);
    while (Serial.available());

	Serial.print("Serial+Ver:");
	Serial.println(SWVersion);

	roboclaw.begin(baudRateToRoboClaw);
	checkRoboClawComms();

	//i2c
	Wire.begin(I2CWIRE_DRIVE_UNIT_ADDRESS);
	Wire.onReceive(receiveEvent);

	//LEDs set to output
	pinMode(MIDDLE_BLUE_LED_PIN, OUTPUT);
	pinMode(BUMPER_BLUE_LED_PIN, OUTPUT);
	pinMode(ORANGE_LED_PIN, OUTPUT);

	//Pull up the bumber switches
	pinMode(BUMPER_L_PIN, INPUT_PULLUP);
	pinMode(BUMPER_R_PIN, INPUT_PULLUP);

	changeState(CurrentState::Stopped);
}

//Make sure the claw is online
void checkRoboClawComms()
{
	delay(300);
	char version[32];
	Serial.print("RC Bd:");
	Serial.println(baudRateToRoboClaw);
	
	if (roboclaw.ReadVersion(RCAddress, version))
	{
		Serial.print("RC:");
		Serial.println(version);
	}
	else
	{
		Serial.println("RCnocom");
	}
}

//Make sure the claw is operating correctly
void checkRoboClawStatus()
{
	uint16_t temp = 0;
	bool valid = true;
	
	temp = roboclaw.ReadError(RCAddress, &valid);
	if (valid == true)
	{
		Serial.println("RCOK");
	}
	else
	{
		Serial.print("RCEr:");
		Serial.println(temp);
	}

	if (roboclaw.available() == true)
	{
		Serial.println("RCok");
		temp = 0;
	}
	else
	{
		Serial.println("RC NoTav");
	}
	
	Serial.print("Temp:");
	if (roboclaw.ReadTemp(RCAddress, temp) == true)
	{
		Serial.println(temp);
	}
	else
	{
		Serial.println("ER");
	}

	temp = roboclaw.ReadMainBatteryVoltage(RCAddress, &valid);
		
	Serial.print("RCbat:");
	if (valid)
	{
		Serial.println(temp * 0.1);
	}
	else
	{
		Serial.println("ER");
	}

	temp = roboclaw.ReadLogicBatteryVoltage(RCAddress, &valid);
	Serial.print("Logic:");
	if (valid)
	{
		Serial.println(temp * 0.1);
	}
	else
	{
		Serial.println("ER");
	}
}

//Print out the system status
void systemStatus()
{
	Serial.print("V:");
	Serial.println(SWVersion);

	checkRoboClawComms();
	checkRoboClawStatus();
	Serial.println(getStateString());
	printStatus();
}

void setCurrentMALTurningSpeed(int speed, String func)
{
#ifdef DEBUG
	Serial.print("CrntTSp:");
	Serial.println(speed);
	Serial.print("F:");
	Serial.println(func);
#endif
	CurrentMALTurningSpeed = MAXIMUM_SPEED;

	if (speed < MAXIMUM_SPEED)
		CurrentMALTurningSpeed = speed;

#ifdef DEBUG
	Serial.print("NwCtTSp:");
	Serial.println(CurrentMALTurningSpeed);
#endif
}

void deadLeft()
{
	if (MAL_CurrentState == MovingForward || MAL_CurrentState == MovingBackward || MAL_CurrentState == DeadTurningRight)
		dropDownToStop();
	changeState(CurrentState::DeadTurningLeft);
	if (CurrentMALSpeed == 0)
		limitSpeed(FIRST_DEAD_TURN_SPEED);

	justMoveDeadLeft();
	setCurrentMALTurningSpeed(0, "deadLeft");
}

void justMoveDeadLeft()
{
	roboclaw.ForwardM1(RCAddress, CurrentMALSpeed); //start Motor1 forward 
	roboclaw.BackwardM2(RCAddress, CurrentMALSpeed); //start Motor2 backward 
}

void deadRight()
{
	if (MAL_CurrentState == MovingForward || MAL_CurrentState == MovingBackward || MAL_CurrentState == DeadTurningLeft)
		dropDownToStop();

	changeState(CurrentState::DeadTurningRight);

	if (CurrentMALSpeed == 0)
		limitSpeed(FIRST_DEAD_TURN_SPEED);

	justMoveDeadRight();
	setCurrentMALTurningSpeed(0, "deadRight");
}

void justMoveDeadRight()
{
	roboclaw.ForwardM2(RCAddress, CurrentMALSpeed); //start Motor2 forward 
	roboclaw.BackwardM1(RCAddress, CurrentMALSpeed); //start Motor1 backward 
}

//When stepping down speeds so no sudden jerk to zero
//we step down in x num steps, this is the time between those steps
#define TIME_BETWEEN_STEP_DOWN_SPEED 150

//Number of steps to reduce the speed when changing direction
#define NUM_SPEED_CHANGES_DIRECTION 3

void limitSpeed(int newSpeed)
{
	CurrentMALSpeed = newSpeed;

	if (CurrentMALSpeed > MAXIMUM_SPEED)
	{
#ifdef DEBUG
		Serial.print("ER! >SP!:");
		Serial.println(CurrentMALSpeed);
#endif
		CurrentMALSpeed = MAXIMUM_SPEED;
	}

	workOutsafeDistance();
}

void dropDownToStop()
{
#ifdef DEBUG
	Serial.print("->0:" + getStateString());
	Serial.print("Sp:");
	Serial.println(CurrentMALSpeed, DEC);
#endif

	int stepSize = CurrentMALSpeed / NUM_SPEED_CHANGES_DIRECTION;

	for (int step = 0; step < NUM_SPEED_CHANGES_DIRECTION; ++step)
	{
		limitSpeed(CurrentMALSpeed - stepSize);

		resendCurrentMove();

		delay(TIME_BETWEEN_STEP_DOWN_SPEED);
	}

	CurrentMALSpeed = 0;
	resendCurrentMove();
	changeState(CurrentState::Stopped);
	setCurrentMALTurningSpeed(0, "dropDwnTStp");
}

void move()
{
	if (MAL_CurrentState == CurrentState::MovingForward)
	{
		driveForward();
	}
	else if (MAL_CurrentState == CurrentState::MovingBackward)
	{
		driveReverse();
	}
}

void rampUpToSpeed(int originalSpeed)
{
	if (originalSpeed > MAXIMUM_SPEED)
		originalSpeed = MAXIMUM_SPEED;

	int stepSize = originalSpeed / NUM_SPEED_CHANGES_DIRECTION;

	for (int step = 0; step < NUM_SPEED_CHANGES_DIRECTION; ++step)
	{
		limitSpeed(CurrentMALSpeed + stepSize);
		move();
	}
	limitSpeed(originalSpeed);
	move();
	setCurrentMALTurningSpeed(0, "rampUpToSpeed");
}

//Move forward
void driveForward()
{
#ifdef DEBUG
	Serial.print("FWD to RC:");
	Serial.println(CurrentMALSpeed);
#endif

	roboclaw.ForwardM1(RCAddress, CurrentMALSpeed); //start Motor1 forward 
	roboclaw.ForwardM2(RCAddress, CurrentMALSpeed); //start Motor1 forward 
}


//move in reverse
void driveReverse()
{
#ifdef DEBUG
	Serial.print("RVS to RC:");
	Serial.println(CurrentMALSpeed);
#endif
	roboclaw.BackwardM1(RCAddress, CurrentMALSpeed); //start Motor1 backward 
	roboclaw.BackwardM2(RCAddress, CurrentMALSpeed); //start Motor2 backward 
}

void reverse()
{
	//Dont just ram it in opposite direction - speed should drop gradually
	if (MAL_CurrentState == CurrentState::MovingForward)
	{
#ifdef DEBUG
		Serial.println("REVwasFWD");
#endif

		int OriginalSpeed = CurrentMALSpeed;

		//Now speed back up in reverse
		dropDownToStop();
		changeState(CurrentState::MovingBackward);

#ifdef DEBUG
		Serial.println("Sw2REV-rampingreverse");
#endif

		rampUpToSpeed(OriginalSpeed);
	}
	else
	{
		if (CurrentMALSpeed == 0)
			limitSpeed(FIRST_SPEED);

		changeState(CurrentState::MovingBackward);

		driveReverse();
	}
}

void forward()
{
	//Dont just ram it in opposite direction - speed should drop gradually
	if (MAL_CurrentState == CurrentState::MovingBackward)
	{
#ifdef DEBUG
		Serial.println("RV->F");
#endif

		int OriginalSpeed = CurrentMALSpeed;

		//Now speed back up in reverse
		dropDownToStop();
		changeState(CurrentState::MovingForward);

#ifdef DEBUG
		Serial.println("FramF");
#endif

		rampUpToSpeed(OriginalSpeed);
	}
	else
	{
		if (CurrentMALSpeed == 0)
			CurrentMALSpeed = FIRST_SPEED;
		
		changeState(CurrentState::MovingForward);

		driveForward();
	}
}

//Change the speed
void changeSpeed(int adjust)
{
	limitSpeed(CurrentMALSpeed + adjust);

	if (CurrentMALSpeed == 0)
	{
		changeState(Stopped);
	}

	resendCurrentMove();

#ifdef DEBUG
	Serial.print("Sp:");
	Serial.println(CurrentMALSpeed);
#endif
}

void resendCurrentMove()
{
	if (MAL_CurrentState == CurrentState::MovingForward)
		driveForward();
	else if (MAL_CurrentState == CurrentState::MovingBackward)
		driveReverse();
	else if (MAL_CurrentState == CurrentState::DeadTurningLeft)
		justMoveDeadLeft();
	else if (MAL_CurrentState == CurrentState::DeadTurningRight)
		justMoveDeadRight();
}

void Stop()
{
#ifdef DEBUG
	Serial.println("NSTP:" + getStateString());
#endif
	limitSpeed(CurrentMALSpeed * 0.5);
	resendCurrentMove();
	delay(100);
	CurrentMALSpeed = 0;
	resendCurrentMove();
	changeState(Stopped);
	setCurrentMALTurningSpeed(0, "Stop");
}

//MAL needs to stop right now !
void ESTOP()
{
#ifdef DEBUG
	Serial.println("XXESp");
#endif
	CurrentMALSpeed = 0;
	driveForward();
	changeState(EStopped);
	setCurrentMALTurningSpeed(0, "ESTOP");
}

//Bumper was hit
void hitBumper(bool /*left*/)
{
	ESTOP();
	signal(9, true);
}

//See if the bumber has hit something
void testBumpers()
{
	//If no switch pressed on bumper - return
	if (digitalRead(BUMPER_L_PIN) != HIGH || digitalRead(BUMPER_R_PIN) != HIGH)
	{
		bool left = false;
		if (digitalRead(BUMPER_L_PIN) == LOW)
		{
			left = true;
			Serial.println("BpL");
		}
		else if (digitalRead(BUMPER_R_PIN) == LOW)
		{
			Serial.println("BpR");
		}

		hitBumper(left);
	}
}

//This is used to do things every X loop calls/cycles
static int LOOPCounter = 0;

//Try and recover from crashing state
//true if we resolve the crash - false if not 
void resolveCd()
{
	if (digitalRead(BUMPER_L_PIN) == HIGH && digitalRead(BUMPER_R_PIN) == HIGH)
	{
		changeState(Stopped);
#ifdef DEBUG
		Serial.println(">Crs");
#endif
		setCurrentMALTurningSpeed(0, "resolveCd");
	}
}

#ifdef TEST
void UnitTests()
{
	//Test me
	Serial.print("Min is 1 2 :");
	Serial.println(getMin(1, 2));

	Serial.print("Min is 1 0 :");
	Serial.println(getMin(1, 0));

	Serial.print("Min is 2 0 :");
	Serial.println(getMin(2, 0));

	Serial.print("Min is 0 0 :");
	Serial.println(getMin(0,0));

	Serial.print("Min is 2 1 :");
	Serial.println(getMin(2, 1));

	Serial.print("Min is 2 2 :");
	Serial.println(getMin(2, 2));

	Serial.print("Min is 9 9 :");
	Serial.println(getMin(9, 9));

	Serial.print("Min is 1 2 3 :");
	Serial.println(getMin(1, 2, 3));
	Serial.print("Min is 3 2 1 :");
	Serial.println(getMin(3, 2, 1));
	Serial.print("Min is 0 0 0 :");
	Serial.println(getMin(0, 0, 0));
	Serial.print("Min is 1 1 1 :");
	Serial.println(getMin(1, 1, 1));
	Serial.print("Min is 1 1 2 :");
	Serial.println(getMin(1, 1, 2));
	Serial.print("Min is 2 2 1 :");
	Serial.println(getMin(2, 2, 1));
	Serial.print("Min is 1 0 0 :");
	Serial.println(getMin(1, 0, 0));
	Serial.print("Min is 1 2 0 :");
	Serial.println(getMin(1, 2, 0));
	Serial.print("Min is 99 88 77 :");
	Serial.println(getMin(99, 88, 77));
}
#endif

bool movingFB()
{
	if (MAL_CurrentState == CurrentState::MovingBackward ||
		MAL_CurrentState == CurrentState::MovingForward)
		return true;
	return false;
}

//Ubiquitous
void loop()
{	
	LOOPCounter++;

	//If we have crashed then resolve it
	if (MAL_CurrentState == CurrentState::EStopped)
	{
		resolveCd();
		return;
	}
	else 
	{
		testBumpers();
		
		if (movingFB())
			checkSafeDistance();

		checkIncoming();

		if (USE_MDM && LOOPCounter % SendStatsEveryXLoops == 0)
			sendStatsToMDM();
	}

	if (LOOPCounter > 1000)
	{
		LOOPCounter = 0;
	}
	
	delay(LOOP_DELAY);
}

void steer(bool left)
{
	//it doesnt matter the direction we are going in forw or back
	//always slow the motor on the side we are turning, ie slow the right motor
	//when going right
	if (CurrentMALTurningSpeed == 0)
		setCurrentMALTurningSpeed(CurrentMALSpeed - TURN_SPEED_SLOW_DOWN, "steer");
	else
		setCurrentMALTurningSpeed(CurrentMALTurningSpeed - TURN_SPEED_SLOW_DOWN, "steer");

#ifdef DEBUG
	if (left)
		Serial.print("LtSp:");
	else
		Serial.print("RtSp:");
	Serial.print(CurrentMALSpeed);
	Serial.print(":TnSp:");
	Serial.println(CurrentMALTurningSpeed);
#endif

	if (MAL_CurrentState == CurrentState::MovingForward)
	{
		if (left)
		{
			roboclaw.ForwardM1(RCAddress, CurrentMALSpeed); 
			roboclaw.ForwardM2(RCAddress, CurrentMALTurningSpeed); 
		}
		else
		{
			roboclaw.ForwardM1(RCAddress, CurrentMALTurningSpeed); 
			roboclaw.ForwardM2(RCAddress, CurrentMALSpeed); 
		}
	}
	else
	{
		if (left)
		{
			roboclaw.BackwardM1(RCAddress, CurrentMALSpeed); 
			roboclaw.BackwardM1(RCAddress, CurrentMALTurningSpeed); 
		}
		else
		{
			roboclaw.BackwardM1(RCAddress, CurrentMALTurningSpeed); 
			roboclaw.BackwardM1(RCAddress, CurrentMALSpeed); 
		}
	}
}

void steerLeft()
{
	steer(true);
}

void steerRight()
{
	steer(false);
}

//see whats coming down bluetooth or serial comms, they both use the same chanell (hardware serial), so its one or the other
void checkIncoming()
{
	//can be either BT or serial
	if (Serial.available())
	{
		char ch = Serial.read();

#ifdef DEBUG
		Serial.print("cB:");
		Serial.println(ch);
#endif 

		if (ch == '1')
		{
			signal(5, true, true, true);
		}
		else if (ch == '0')
		{
			resetLEDs();
		}
		else if (ch == 'f')
		{
			forward();
		}
		else if (ch == 'r')
		{
			reverse();
		}
		else if (ch == '+')
		{
			if (MALmode != MALMode::Kid)
				changeSpeed(SPEED_CHANGE_AMOUNT);
		}
		else if (ch == '-')
		{
			if (MALmode != MALMode::Kid)
				changeSpeed(0-SPEED_CHANGE_AMOUNT);
		}
		else if (ch == 'x')
		{
			Stop();
		}
		else if (ch == 'v')
		{
			checkRoboClawComms();
		}
		else if (ch == 's')
		{
			systemStatus();
		}
		else if (ch == 'd')
		{
			if (MALmode != MALMode::Kid)
				USE_MDM = true;
		}
		else if (ch == 'o')
		{
			USE_MDM = false;
		}
		else if (ch == '<')
		{
			if (MALmode != MALMode::Kid)
				deadLeft();
		}
		else if (ch == '>')
		{
			if (MALmode != MALMode::Kid)
				deadRight();
		}
		else if (ch == ',')
		{
			steerLeft();
		}
		else if (ch == '.')
		{
			steerRight();
		}
		else if (ch == 'A')
		{
			if (MALmode != MALMode::Kid)
				MALmode = MALMode::Auto;
		}
		else if (ch == 'K')
		{
			MALmode = MALMode::Kid;
		}
		else if (ch == 'M')
		{
			if (MALmode != MALMode::Kid)
				MALmode = MALMode::Manual;
		}
		else if (ch == '9')
		{
			MALmode = MALMode::Manual;
		}
		else
		{
			//Print to BT
			printCommands();
		}
		//dump the char if its the same inside this loop - 
		//this is because it gets repeated from the BT app - 
		//otherwise we end up doing things twice !
		char newch = Serial.peek();
		if (newch == ch)
		{
#ifdef DEBUG
			Serial.print("DupChr");
			Serial.println(newch);
#endif 
			newch = Serial.read();
		}
	}
}

//Display the commands to the user
void printCommands()
{
	Serial.println("Cd");
	Serial.println("sSy");
	Serial.println("1L+");
	Serial.println("0L-");
	Serial.println("fF");
	Serial.println("rRv");
	Serial.println("+Sp");
	Serial.println("-Sp");
	Serial.println("xStp");
	Serial.println("<DL");
	Serial.println(">DR");
	Serial.println(",LSr");
	Serial.println(".RSr");
	Serial.println("dMDM");
	Serial.println("o-MDM");
	Serial.println("AAut"); //24
	Serial.println("KKid"); //22
	Serial.println("MMan"); //23
	Serial.println("9KM-"); //21
}
