# README #

This is the code for the Robotics For Dreamers web site. It contains all the modules for the actual production ready robot that I have discussed so far. Also included are the MOB BT/i2c debug module, and the Arduino/Genuino 101 Neural Network code.

All written by 

Marcus O'Brien
marcusobrien@yahoo.com

http://www.roboticsfordreamers.com/

http://www.electricalengineeringschools.org/2016/computer-engineering-and-the-hidden-neural-network-brain-in-the-arduino-101/