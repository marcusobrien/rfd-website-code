#include <NewPing.h>
#include <Wire.h>
#include <WString.h>
#include "I2CMyNetwork.h"

/* I2C Commands
Object Detection Module
"PINGON", // test for objects
"PINGOF", // dont test for objects
"TELLMM", // tell the mobility module the measurements via i2c
"TELLOF", // dont tell the mobility module the measurements via i2c
"DEBGON", // turn debug on - send to the MDM
"DEBGOF", // turn off debug - dont send anything fown i2c to MDM
"HELPCM"  //List of commands

MOB Debug Module  Commands
SAVESD    - Save to SD card
SENDBT    - Send all across wire via Bluetooth
CLEARS    - Clear the mem
SCRLON    - Scroll mode on
SCRLOF    - Scroll mode off
LIVEON    - send every event up across bluetooth for live monitoring
LIVEOF    - send every event up across bluetooth for live monitoring
LCDONN    - Turn the screen on
LCDOFF    - Turn the screen off (save battery)
HELPCM    - Help commands
SENDI2    - Send out the following comamnd on i2c - the format is COM-SENDI2-ADDRESS-COMMAND ie COM-SENDI2-008-PING
VSTATS    - View stats on both BT and the LCD
*/

String version("ObjD-PROD V6.00");

//Distance at which to consider an obsticle is in the way - at the lowest speed
//This is used to stop driving and also to light up LEDs
#define SAFE_OBJECT_DISTANCE 35

//This is the distance below which we will consider an invalid metric - ie 0 is not a valid distanCe, or 1 cm etc
#define MIN_VALID_DISTANCE 5

#define I2CWIRE_DRIVE_UNIT_ADDRESS 1
#define I2CWIRE_OBJECT_DETECT_ADDRESS 2
#define I2CWIRE_101_NN_ADDRESS 69
#define I2CWIRE_MDM_ADDRESS 9
#define I2C_WIRE_DELAY 15

#define NUM_SONICS 9

//#define DEBUG 1

#define LED_TEST_DELAY 2000

//EStopped - so we force stopped ie crashed/hit bumper, or the estop was pushed - we need to recover from this
//Stopped - we have stopped moving - speed hit zero
//Moving - Speed > 0
enum CurrentState
{
	EStopped = 0,
	MovingForward = 1,
	MovingBackward = 2,
	DeadTurningLeft = 3,
	DeadTurningRight = 4,
	Stopped = 5
}MAL_CurrentState;

//Print info out - measurements etc -debug and prod mode
bool DumpInfo = false;

void sendItI2c(const int address, const char *message)
{
	String msg("Tx I2C Add:");

  if (DumpInfo)
  {
  	switch (address)
  	{
  	case I2CWIRE_DRIVE_UNIT_ADDRESS:
  #ifdef DEBUG
  		Serial.print(msg);
  		Serial.println(I2CWIRE_DRIVE_UNIT_ADDRESS);
  #endif
  		break;
  	case I2CWIRE_OBJECT_DETECT_ADDRESS:
  #ifdef DEBUG
  		Serial.print(msg);
  		Serial.println(I2CWIRE_OBJECT_DETECT_ADDRESS);
  #endif
  		break;
  	case I2CWIRE_MDM_ADDRESS:
  #ifdef DEBUG
  		Serial.print(msg);
  		Serial.println(I2CWIRE_MDM_ADDRESS);
  #endif
  		break;
  	default:
  #ifdef DEBUG
  		Serial.print("ERR I2C.Bad Add:");
  		Serial.println(address);
  #endif
  		return;
  	}
  }
  
	if (message == NULL || strlen(message) < 1)
	{
#ifdef DEBUG
		Serial.println("ERR I2C-Len");
#endif
	}
	else
	{
		Wire.beginTransmission(address);
		Wire.write(message, strlen(message));
		Wire.endTransmission();
   
    if (DumpInfo)
    {
  		Serial.println("I2C Msg:-");
  		Serial.println(message);
  		Serial.println("I2C Msg Sent");
    }
	}
}


//Turn this on/off for sending i2c to the MDM unit - dont turn it on if not connected - it freezes the system !
bool USE_MDM = false;

void sendToMDM(String message)
{
	if (!USE_MDM)
		return;

#ifdef DEBUG
	Serial.print("MDM I2C Msg:-");
	Serial.println(message);
#endif

	sendItI2c(I2CWIRE_MDM_ADDRESS, message.c_str());
}

#define PING_PIN 2 // Arduino pin for both trig and echo
#define NUM_SONICS 9
#define MAX_DISTANCE 250

NewPing sonar[NUM_SONICS] = {     // Sensor object array.
	NewPing(PING_PIN, PING_PIN, MAX_DISTANCE), // Each sensor's trigger pin, echo pin, and max distance to ping.
	NewPing(PING_PIN + 1, PING_PIN + 1, MAX_DISTANCE),
	NewPing(PING_PIN + 2, PING_PIN + 2, MAX_DISTANCE),
	NewPing(PING_PIN + 3, PING_PIN + 3, MAX_DISTANCE),
	NewPing(PING_PIN + 4, PING_PIN + 4, MAX_DISTANCE),
	NewPing(PING_PIN + 5, PING_PIN + 5, MAX_DISTANCE),
	NewPing(PING_PIN + 6, PING_PIN + 6, MAX_DISTANCE),
	NewPing(PING_PIN + 7, PING_PIN + 7, MAX_DISTANCE),
	NewPing(PING_PIN + 8, PING_PIN + 8, MAX_DISTANCE)
};

//Help Commands
#define COMMAND_LENGTH 6
#define NUM_OF_COMMANDS 10
const char* ListOfCommands[NUM_OF_COMMANDS] =
{
	"PINGON", // test for objects
	"PINGOF", // dont test for objects
	"TELLMM", // tell the mobility module the measurements via i2c
	"TELLOF", // dont tell the mobility module the measurements via i2c
	"DEBGON", // turn debug on - send to the MDM
	"DEBGOF", // turn off debug - dont send anything fown i2c to MDM
	"STATUS", // Print status of this unit
  "VERBOF", // Stop the print info
  "VERBON", // Resume the print off
	"HELPCM"  //List of commands
};

static const unsigned int COM_PINGON=0;
static const unsigned int COM_PINGOF=1;
static const unsigned int COM_TELLMM=2;
static const unsigned int COM_TELLOF=3;
static const unsigned int COM_DEBGON=4;
static const unsigned int COM_DEBGOF=5;
static const unsigned int COM_STATUS=6;
static const unsigned int COM_VERBOF=7;
static const unsigned int COM_VERBON=8;
static const unsigned int COM_HELPCM=9;

//Send out the ping tests for all 9
bool PINGON = true;

//For sending info to the debug module
bool I2C_DEBUG = false;

//Send out the object distances via i2c
bool I2CON = true;

#define LOOP_FREQUENCY 50
#define DELAY_BETWEEN_PINGS 15

int LastResults[NUM_SONICS];
String I2CAdd("I2C Add:");

void printThisI2CAddress(String msg)
{
  if (DumpInfo)
  {
  	Serial.println(msg);
  	Serial.print(I2CAdd);
  	Serial.println(I2CWIRE_OBJECT_DETECT_ADDRESS);
  }
}

//print out to SM the values of the sensors
void printUS(unsigned int i)
{
  Serial.print("USS#");
  Serial.print(i);
  Serial.print(':');
  Serial.print(LastResults[i]);
  Serial.println("cm");
}

//detetion LEDS - when see an object blink until it goes away
int getLED(int US_Num)
{
  switch (US_Num)
  {
  case 0:
  case 7:
    return PIN_MID_RIGHT;
  case 1:
    return PIN_TOP_RIGHT;
  case 2:
    return PIN_TOP_LEFT;
  case 3:
  case 4:
    return PIN_MID_LEFT;
  case 5:
    return PIN_BOT_LEFT;
  case 6:
    return PIN_BOT_RIGHT;
  default:
    return PIN_TOP_MID;
  }
}

int GetLEDState(int count)
{
  if (LastResults[count] > MIN_VALID_DISTANCE && LastResults[count] < SAFE_OBJECT_DISTANCE)
  {
      return HIGH;
  } 
  return LOW;
}

void testLED(unsigned int ultraSensor)
{
    Serial.print("Tst LED-US#");
    Serial.println(ultraSensor);

    //backup reading
    int originalReading = LastResults[ultraSensor];
    Serial.print("cm:");
    Serial.println(LastResults[ultraSensor]);
      
    //Test LED when US detects valid object
    LastResults[ultraSensor] = SAFE_OBJECT_DISTANCE-1;
    Serial.print("cm:");
    Serial.println(LastResults[ultraSensor]);
    turnLED(getLED(ultraSensor), GetLEDState(ultraSensor));
    delay(LED_TEST_DELAY);

    //Test LED when US detects no objects
    LastResults[ultraSensor] = MIN_VALID_DISTANCE+1;
    Serial.print("cm:");
    Serial.println(LastResults[ultraSensor]);
    turnLED(getLED(ultraSensor), GetLEDState(ultraSensor));
    delay(LED_TEST_DELAY);

    //Test turning back on again
    LastResults[ultraSensor] = SAFE_OBJECT_DISTANCE-1;
    Serial.print("cm:");
    Serial.println(LastResults[ultraSensor]);
    turnLED(getLED(ultraSensor), GetLEDState(ultraSensor));
    delay(LED_TEST_DELAY);

    //restore US reading and LED state
    LastResults[ultraSensor] = originalReading;
    Serial.print("cm:");
    Serial.println(LastResults[ultraSensor]);
    turnLED(getLED(ultraSensor), GetLEDState(ultraSensor));
    
    Serial.print("End LED Tst:");
    Serial.println(ultraSensor);
}

void turnLED(int led_num, int state)
{
  digitalWrite(led_num, state);
  if (DumpInfo)
  {
    if (state)
      Serial.print("ObjDtctn LED ON:");
    else
      Serial.print("ObjDtctn LED OFF:");
    Serial.println(led_num);
  }
}

void systemStatus(bool fullStatus = false)
{
  Serial.println("STATUS");
	Serial.println(version);
	printThisI2CAddress("Setup I2C");
	Serial.print(NUM_SONICS, DEC);
	Serial.println(" US Sensors");
	Serial.print(MAX_DISTANCE, DEC);
	Serial.println("cm Max Dist");

	Serial.print(LOOP_FREQUENCY, DEC);
	Serial.println("s Loop Delay");
	Serial.print(DELAY_BETWEEN_PINGS, DEC);
	Serial.println("s Ping Sep");
	Serial.print(I2C_WIRE_DELAY, DEC);
	Serial.println("s Wire Sep");
  Serial.print("DEBUG:");
#ifdef DEBUG
	Serial.println("ON");
#else
	Serial.println("OFF");
#endif
  Serial.print("DumpInfo:");
  if (DumpInfo)
  {
    Serial.println("ON"); 
  }
  else
  {
    Serial.println("OFF");
  }

  Serial.println("STATUS CHECK PINs+LEDs");
  for (unsigned int ultraSensor = 0; ultraSensor < NUM_SONICS; ultraSensor++)
  {
      printUS(ultraSensor);
      if (fullStatus)
      {
        testLED(ultraSensor);
        delay(LED_TEST_DELAY);
      }
      else
      {
        //just flash the led quickly for power on
        turnLED(getLED(ultraSensor), HIGH);
        delay(LED_TEST_DELAY*0.5);
        turnLED(getLED(ultraSensor), LOW);
        delay(LED_TEST_DELAY*0.5);
        turnLED(getLED(ultraSensor), HIGH);
        delay(LED_TEST_DELAY*0.5);
      } 
  }
  Serial.println("Status Check is complete");
}

void setup() 
{
	Serial.begin(9600);

	pinMode(PIN_MID_RIGHT, OUTPUT);
	//US 1
	pinMode(PIN_TOP_RIGHT, OUTPUT);
	//US 2
	pinMode(PIN_TOP_LEFT, OUTPUT);
	//US 3 AND US 4
	pinMode(PIN_MID_LEFT, OUTPUT);
	//US 5
	pinMode(PIN_BOT_LEFT, OUTPUT);
	//US 6
	pinMode(PIN_BOT_RIGHT, OUTPUT);
	//US 8
	pinMode(PIN_TOP_MID, OUTPUT);

	Wire.begin(I2CWIRE_OBJECT_DETECT_ADDRESS);
	Wire.onReceive(receiveEvent);

#ifdef DEBUG
	Serial.println("Setup Finished");
#endif
  systemStatus();
}

void sendObjectDetectionInfoToi2c(int address)
{
	//The length of these tokens does not include a /0
  const char *START_TOKEN = "START:";
  const char *END_TOKEN = "END:";

  if (DumpInfo)  
  {
    printThisI2CAddress("Sending USs");
  }

  sendItI2c(address, START_TOKEN);

	for (int i = 0; i < NUM_SONICS; i++)
	{
		delay(I2C_WIRE_DELAY); 
		int distance = LastResults[i];
    
		//The format is 1 decimal for sensor (as there are <9, and 3 sig places for dist as < 400
		//So the length of this string is ok, even though there are % in it
		char message[] = "US:%d=00%d";
		
		if (distance < 10)
			sprintf(message, "US:%d=00%d", i, distance);
		else if (distance < 100)
			sprintf(message, "US:%d=0%d", i, distance);
		else
			sprintf(message, "US:%d=%d", i, distance);

		sendItI2c(address, message);

    if (DumpInfo)
    {
  		Serial.print("i2c mess:");
  		Serial.println(message);
  
  		Serial.print("Pin:");
  		Serial.println(i);
  		
  		Serial.print("Obj:");
  		Serial.print(distance);
  		Serial.println("cm");
    }
	}
	
	sendItI2c(address, END_TOKEN);

  if (DumpInfo)
  {
	  printThisI2CAddress("Sent i2c");
  }
}

void sortOutLEDS()
{
	//Too near distance - time
	for (int count = 0; count < NUM_SONICS; count++)
	{
		int state = GetLEDState(count);

		 if (DumpInfo == true)
    {
      Serial.println("LEDs");
  		Serial.print("US#");
  		Serial.print(count, DEC);
  		Serial.print(" D:");
  		Serial.print(LastResults[count]);
  		Serial.print("cms");
  		if (state)
  			Serial.println("ON");
  		else
  			Serial.println("OFF");
    }
		turnLED(getLED(count), state);
	}
}

void handleCommand(String command, boolean cameFromi2c = true)
{
	command.trim();
	command.toUpperCase();

  
  if (command.compareTo(ListOfCommands[COM_VERBON])==0)
  {
    DumpInfo = true;
#ifdef DEBUG
    Serial.println("Verbosity ON");
#endif
  }
  else if (command.compareTo(ListOfCommands[COM_VERBOF])==0)
  {
    DumpInfo = false;
#ifdef DEBUG
    Serial.println("Verbosity OFF");
#endif    
  }
	else if (command.compareTo(ListOfCommands[COM_PINGON])==0)
	{
#ifdef DEBUG
		Serial.println("Ping on");
#endif
		PINGON = true;
	}
	else if (command.compareTo(ListOfCommands[COM_PINGOF])==0)
	{
#ifdef DEBUG
		Serial.println("Ping off");
#endif
		PINGON = false;
	}
	else if (command.compareTo(ListOfCommands[COM_DEBGON])==0)
	{
#ifdef DEBUG
		Serial.println("MDM on");
#endif
		I2C_DEBUG = true;
	}
	else if (command.compareTo(ListOfCommands[COM_DEBGOF])==0)
	{
#ifdef DEBUG
		Serial.println("MDM off");
#endif
		I2C_DEBUG = false;
	}
	else if (command.compareTo(ListOfCommands[COM_TELLMM])==0)
	{
#ifdef DEBUG
		Serial.println("i2c to drive unit ON");
#endif
		I2CON = true;
	}
	else if (command.compareTo(ListOfCommands[COM_TELLOF])==0)
	{
#ifdef DEBUG
		Serial.println("i2c 2 Drv Unit Off");
#endif
		I2CON = false;
	}
	else if (command.compareTo(ListOfCommands[COM_STATUS])==0)
	{
#ifdef DEBUG
		Serial.println("Status");
#endif
		systemStatus(true);
	}
	else if (command.compareTo(ListOfCommands[COM_HELPCM])==0)
	{
		if (cameFromi2c)
			sendItI2c(I2CWIRE_MDM_ADDRESS, "ODM Commands");

		for (int i = 0; i < NUM_OF_COMMANDS; i++)
		{
			if (cameFromi2c)
			{
				sendItI2c(I2CWIRE_MDM_ADDRESS, ListOfCommands[i]);
			}
			else
			{
				Serial.println(ListOfCommands[i]);
			}
		}
	}
	else
	{
		String mes("ODM-Invd Comm:");
		//error
		if (cameFromi2c)
		{
#ifdef DEBUG
			Serial.println(mes);
#endif
			sendItI2c(I2CWIRE_MDM_ADDRESS, mes.c_str());
		}
		else
		{
			Serial.println(mes);
			for (int i = 0; i < NUM_OF_COMMANDS; i++)
			{
				Serial.println(ListOfCommands[i]);
			}
		}
		return;
	}
	//Did a valid command
	sendItI2c(I2CWIRE_MDM_ADDRESS, "Valid Command Done");
}

//This is from i2c only !!
void receiveEvent(int bytes)
{
	String newString = "";

	if (bytes < 1)
	{
		newString = "Error";
	}

	//Read from i2C, the new string in the buffer, one char at a time.
	for (int charNum = 0; charNum < bytes; charNum++)
	{
      char newCharacter = Wire.read();

#ifdef DEBUG
		Serial.print("Char i2c:");
		Serial.println(newCharacter);
#endif

		newString = newString + newCharacter;
	}

	handleCommand(newString);
}

//Call this below from loop to test out one sensor
/*
void testSensor()
{
	int USS_To_Test = 3;
	//make sure its not zero in the sensor !!

	delay(50);
	int d = sonar[USS_To_Test].ping() / US_ROUNDTRIP_CM;
	if (d < SAFE_OBJECT_DISTANCE)
	turnLED(PIN_MID_LEFT, HIGH);
	else
	turnLED(PIN_MID_LEFT, LOW);
	Serial.print("USS#:");
	Serial.print(USS_To_Test, DEC);
	Serial.print(':');
	Serial.print(d, DEC);
	Serial.println(" cms");
	return;
}
*/

void loop()
{
	String readString;

	while (Serial.available())
	{
		if (Serial.available() > 0)
		{
			char c = Serial.read();
			readString.concat(c);
#ifdef DEBUG
			Serial.println("New char SM:");
			Serial.println(c);
			Serial.println("Sofar:");
			Serial.println(readString);
#endif
		}
	}

  //Test command
	if (readString.length() > 0)
	{
#ifdef DEBUG
		Serial.println("Msg len SM:");
		Serial.println(readString.length());
		Serial.print("Msg is:");
		Serial.println(readString);
#endif
		if (readString.length() == COMMAND_LENGTH)
		{
#ifdef DEBUG
			Serial.print("Command:");
			Serial.println(readString);
#endif
			handleCommand(readString, false);
		}
		else
		{
			Serial.println("Valid Cmds:");
			for (int i = 0; i < NUM_OF_COMMANDS; i++)
			{
				Serial.println(ListOfCommands[i]);
			}
		}
	}

	if (PINGON == true)
	{
		static int counter = 1;
    if (DumpInfo)
    {
		  Serial.print("Ping.#");
		  Serial.println(counter);
    }

		counter++;
		for (unsigned int i = 0; i < NUM_SONICS; i++)
		{
			delay(DELAY_BETWEEN_PINGS); 
			LastResults[i] = sonar[i].ping() / US_ROUNDTRIP_CM;
      if (DumpInfo == true)
      {			      
  			printUS(i);
      }
		}

		//Send all the results up the wire !
		if (I2CON == true)
		{
			//Send it the the mobility unit
			sendObjectDetectionInfoToi2c(I2CWIRE_DRIVE_UNIT_ADDRESS);
		}

		if (I2C_DEBUG == true)
		{
			//Send it to the debug unit
			sendObjectDetectionInfoToi2c(I2CWIRE_MDM_ADDRESS);
		}

		sortOutLEDS();

		delay(LOOP_FREQUENCY);
	}
}
