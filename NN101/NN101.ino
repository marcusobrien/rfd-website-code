/*MAXIMUM NEURON SIZE IS 128, therefore training vector and testing vector must be less than 128 */


#include <SD.h>
#include <CurieNeuronsPro.h>
#include <SPI.h>

const int chipSelect = 10;   // 4 for Arduino boards, might be different in other non-Arduino boards.
static const char *VERSION = "101-NN DEV v1.1.13";

bool NNTYPEKNN = true;

CurieNeurons hNN;

static const int numberImagesToTrain = 200;
int neuronsAvailable, neuronsCommitted, neuronSize;
int ncr, dist, cat, aif, nid, nsr;
static const int SerialBAUD = 9600;
static const int MAX_COMMANDS_HISTORY_LEN=30;
int nextCommand = 0;

#define NUMBER_ITERATIONS_LEARN_SAME_IMAGE 1

char COMMANDS_LIST[MAX_COMMANDS_HISTORY_LEN];

#define INITIAL_CATEGORY 99
#define NETWORK_CONTEXT 10
#define NN_FORGET 500
#define LED_OUT 13
#define MAX_NEURONS 128
#define NUM_TO_TEST 500
#define BLINK_RATE 1000
#define NUM_IMAGES_TYPES 10

File *dataFile = NULL;
File *labelFile = NULL;

static const int ORIG_ROW = 28;
static const int ORIG_COL = 28;
static const int LEN = ORIG_ROW * ORIG_COL;

static const char *TestImagesDataFileFN = "TESTI.DAT";
static const char *TestLabelsFileFN = "TESTL.DAT";
static const char *TrainImagesDataFileFN = "TRAINI.DAT";
static const char *TrainLabelsFileFN = "TRAINL.DAT";

//#define DEBUG
#define PROCESS_FILE_DEBUG

int MAX_LABEL_NUM_TO_TRAIN = 3;

void checkSDCard()
{
  Serial.println("Checking the SD card.");
  
  Sd2Card card;
  SdVolume volume;
  SdFile root;

  if (!card.init(SPI_HALF_SPEED, chipSelect)) 
  {
    Serial.println("Initialization failed. Things to check:");
    Serial.println("* is a card is inserted?");
    Serial.println("* Is your wiring correct?");
    Serial.println("* did you change the chipSelect pin to match your shield or module?");
    return;
  } 
  else 
  {
    Serial.println("Wiring is correct and a card is present."); 
  }

  Serial.print("Card type: ");
  switch(card.type()) 
  {
    case SD_CARD_TYPE_SD1:
      Serial.println("SD1");
      break;
    case SD_CARD_TYPE_SD2:
      Serial.println("SD2");
      break;
    case SD_CARD_TYPE_SDHC:
      Serial.println("SDHC");
      break;
    default:
      Serial.println("Unknown");
  }

  if (!volume.init(card)) 
  {
    Serial.println("Could not find FAT16/FAT32 partition.nMake sure you've formatted the card");
    return;
  }

  uint32_t volumesize;
  Serial.print("Volume type is FAT");
  Serial.println(volume.fatType(), DEC);
  Serial.println();

  volumesize = volume.blocksPerCluster();    // clusters are collections of blocks
  volumesize *= volume.clusterCount();       // we'll have a lot of clusters
  volumesize *= 512;                            // SD card blocks are always 512 bytes
  Serial.print("Volume size (bytes): ");
  Serial.println(volumesize);
  Serial.print("Volume size (Kbytes): ");
  volumesize /= 1024;
  Serial.println(volumesize);
  Serial.print("Volume size (Mbytes): ");
  volumesize /= 1024;
  Serial.println(volumesize);
  
  Serial.println("Files found on the card (name, date and size in bytes): ");
  root.openRoot(volume);
  
  root.ls(LS_R | LS_DATE | LS_SIZE);

  Serial.println("Checking files");
  
  if (SD.exists(TrainImagesDataFileFN) == true)
  {
    Serial.println("Found the train data file.");
  }
  else
  {
    Serial.println("Could NOT find the training data file !!");
  }  

  if (SD.exists(TrainLabelsFileFN) == true)
  {
    Serial.println("Found the train labels file.");
  }
  else
  {
    Serial.println("Could NOT find the train labels file !!");
  }  

  if (SD.exists(TestImagesDataFileFN) == true)
  {
    Serial.println("Found the test data file.");
  }
  else
  {
    Serial.println("Could NOT find the test data file !!");
  }  
  
  if (SD.exists(TestLabelsFileFN) == true)
  {
    Serial.println("Found the test labels file.");
  }
  else
  {
    Serial.println("Could NOT find the test labels file !!");
  }  
  
  SD.begin(chipSelect);
}

void setup()
{
  pinMode(chipSelect, OUTPUT);
  digitalWrite(chipSelect, HIGH);
  
  Serial.begin(SerialBAUD);
 
  Serial.println("Hello.........................");
 
	while (!Serial);

	Serial.println(VERSION);
  
  SD.begin(chipSelect);

  setNNOptions();
}

void clearNN()
{
  Serial.println("Clearing NN !!");
  hNN.clearNeurons();
  setNNOptions();
}

void setNNType()
{
  //set the knn
  if (NNTYPEKNN)
  {
    hNN.setKNN(); 
  }
  else
  {
    hNN.setRBF();
  }
}

void setNNOptions()
{
  Serial.println("Setting the NN options");
  
  Serial.print("Network Context:");
  Serial.println(NETWORK_CONTEXT);
  
  hNN.GCR(NETWORK_CONTEXT);
  
  setNNType();
}

void toggleNNType()
{
  if (NNTYPEKNN)
  {
    NNTYPEKNN = false;
    Serial.println("Just set the NN to RBF mode");
  }
  else
  {
    NNTYPEKNN = true;
    Serial.println("Just set the NN to KNN mode");
  }
  
  setNNType();
}

void NNInfo()
{
  hNN.getNeuronsInfo(&neuronSize, &neuronsAvailable, &neuronsCommitted);
  Serial.print("Neuron size ="); Serial.println(neuronSize);
  Serial.print("Neurons available ="); Serial.println(neuronsAvailable);
  Serial.print("Neurons committed ="); Serial.println(neuronsCommitted);
  Serial.print("Network Context ="); Serial.println(hNN.GCR());
  if (NNTYPEKNN)
  {
    Serial.println("The NN is in KNN mode");
  }
  else
  {
    Serial.println("The NN is in RBF mode");
  }
  Serial.print("The NN Super test will train and test up to label :");
  Serial.println(MAX_LABEL_NUM_TO_TRAIN);
}

void trainNN()
{
  openTrainingDataFiles();
  hNN.begin();
  hNN.GCR(NETWORK_CONTEXT);
  Serial.println("Training the NN");
  trainNetwork();
  Serial.println("Finished training the NN !!"); 
}

void saveNN()
{
  Serial.println("Saving the NN");
  unsigned char neurons[MAX_NEURONS];
  //hNN.readNeurons(neurons);

  Serial.println("Read the full NN for saving.");
  //Open file and write neurons to it
  
  //hNN.writeNeurons(neurons);
  //blink();
  Serial.println("Saved the full NN.");
}

void openTestingDataFiles()
{
  SD.begin(chipSelect);
  
  dataFile = new File(SD.open(TestImagesDataFileFN));
  if (!dataFile)
  {
    Serial.print("Failed to open the testing data file:");
    Serial.println(TestImagesDataFileFN);
  }
  else
  {
    Serial.print("Opened the testing data file:");
    Serial.println(TestImagesDataFileFN);

    if (dataFile->available())
    {
      Serial.println("Testing data file available.");
    }
    else
    {
      Serial.println("Testing data file NOT available.");
    }
  }
  
  labelFile = new File(SD.open(TestLabelsFileFN));
  if (!labelFile)
  {
    Serial.print("Failed to open the testing labels file:");
    Serial.println(TestLabelsFileFN);
  }
  else
  {
    Serial.print("Opened the testing labels file:");
    Serial.println(TestLabelsFileFN);

    if (labelFile->available())
    {
      Serial.println("Testing label file available.");
    }
    else
    {
      Serial.println("Testing label file NOT available.");
    }
  }
}

void openTrainingDataFiles()
{
  SD.begin(chipSelect);
  
  dataFile = new File(SD.open(TrainImagesDataFileFN));
  if (!dataFile)
  {
    Serial.print("Failed to open the training data file:");
    Serial.println(TrainImagesDataFileFN);
    closeFiles();
    return;
  }
  else
  {
    Serial.print("Opened the training data file:");
    Serial.println(TrainImagesDataFileFN);

    if (dataFile->available())
    {
      Serial.println("Training data file available.");
    }
    else
    {
      Serial.println("Training data file NOT available.");
      closeFiles();
      return;
    }
  }
  
  labelFile = new File(SD.open(TrainLabelsFileFN));
  if (!labelFile)
  {
    Serial.print("Failed to open the training labels file:");
    Serial.println(TrainLabelsFileFN);
    closeFiles();
    return;
  }
  else
  {
    Serial.print("Opened the training labels file:");
    Serial.println(TrainLabelsFileFN);

    if (labelFile->available())
    {
      Serial.println("Training label file available.");
    }
    else
    {
      Serial.println("Training label file NOT available.");
      closeFiles();
      return;
    }
  }
  
}

void closeFiles()
{
  Serial.println("About to close data and label files.");
  if (dataFile)
  {
    dataFile->close();
    dataFile = NULL;
  }
  if (labelFile)
  {
    labelFile->close();
    labelFile = NULL;
  }
  Serial.println("Closed data and label files.");
}

void printImage(char *message, unsigned char *pattern, int len)
{
  Serial.println(message);
  Serial.print("Image is ");
  for (int index =0; index < len; index++)
  {
    Serial.print(pattern[index]);
    Serial.print(" ");
  }
  Serial.println("  ");
}

int processImage(unsigned char *pattern)
{
#ifdef PROCESS_FILE_DEBUG
  printImage("*********** Image is being converted from  : ", pattern, LEN);
#endif

  //Anything above this pixel value is considered to be on, anything below or equal is off
  int pixelThreshold = 5;
  
  if (pattern)
  {
    //Monochrome the image
    for (int pixel = 0; pixel < LEN; pixel++)
    {
      if (pattern[pixel] >= pixelThreshold)
        pattern[pixel] = 1;
      else
        pattern[pixel] = 0;
    }    

    //We have 28x28 pixels of either on or off
    //so squash it into 98 bytes - so we have converted 784 to 98
    
    int byte_Size = 8;
    int newImageSizeBytes = LEN / byte_Size;
    
    unsigned char newImage [newImageSizeBytes];
    int newImageIndex = 0;
    
    //Or the new byte with MSB first - left to right
    int BitOrValue = 128;
    
    for (int originalImageIndex = 0; originalImageIndex < LEN; originalImageIndex++)
    {
      unsigned int originalPixel = pattern[originalImageIndex];

#ifdef PROCESS_FILE_DEBUG
      Serial.print("Orignal Pixel At (");
      Serial.print(originalImageIndex);
      Serial.print(") Val Was ");
      Serial.print(originalPixel);
#endif
      if (originalPixel > 0)
      {
#ifdef PROCESS_FILE_DEBUG
        Serial.print(" value was not 0 new image pixel changed from ");
        Serial.print(newImage[newImageIndex]);
        Serial.print(" to ");
#endif
        newImage[newImageIndex] | BitOrValue;
#ifdef PROCESS_FILE_DEBUG
        Serial.println(newImage[newImageIndex]);
#endif
      }
      
      //The value we Bitwise OR with goes from 128 to 1, then round again
      if (BitOrValue > 1)
      {
#ifdef PROCESS_FILE_DEBUG
        Serial.print(" shifted the bitwise byte from ");
        Serial.print(BitOrValue);
#endif
        BitOrValue = BitOrValue >> 1;
#ifdef PROCESS_FILE_DEBUG
        Serial.print(" to ");
        Serial.println(BitOrValue);
#endif        
      }
      else
      {
        BitOrValue = 128;
#ifdef PROCESS_FILE_DEBUG
        Serial.println("The bitwise byte was at 1, now its rolled over to 128.");
#endif
      }

      //Are we at the last bit of the byte in the old image ie is the index divisible by 7
      //If it is then move to the next byte in the new image, otherwise dont bother
      if (originalImageIndex % 7)
      {
        if (BitOrValue == 128)
        {
#ifdef PROCESS_FILE_DEBUG
          Serial.println("The bitwise byte was at 128, so move on to next new image pixel.");
#endif
          ++newImageIndex;
        }

#ifdef PROCESS_FILE_DEBUG
        else
        {
          Serial.println("Error ProcessImage : should be on next byte of new image, but havent reset the bitwise OR value to 128."); 
        }
#endif
      }
      pattern = newImage;

#ifdef PROCESS_FILE_DEBUG
      printImage("Image is Now : ", pattern, newImageIndex);
      if (newImageIndex != newImageSizeBytes)
      {
        Serial.println("The new image size was created as ");
        Serial.print(newImageSizeBytes);
        Serial.print(" but we only converted ");
        Serial.print(newImageIndex);
        Serial.println(" bytes.");
      }
#endif
  
    }
    return newImageSizeBytes;
  }
  Serial.println("Failed Process Image - No Image data!");
  return 0;
}

int trainLabelSeekPos = 8;
int trainDataSeekPos = 16;

void trainNetwork()
{
  int images_types[NUM_IMAGES_TYPES];
  clearImagesArray(images_types);

   if (!labelFile || !dataFile)
  {
    Serial.println("Training network failed - files not opened.");
    return;
  }

  if (labelFile->available() == 0)
  {
    Serial.println("Training network failed - label file not available.");
    return;
  }

  if (dataFile->available() == 0)
  {
    Serial.println("Training network failed - data file not available.");
    return;
  }

  int labelNum = 0;
  labelFile->seek(trainLabelSeekPos);
  dataFile->seek(trainDataSeekPos);
  
  int count_images = 0;
  
  unsigned char pattern[LEN]; // values must range between 0-255. Upper byte is discarded by CM1K
  hNN.GCR(NETWORK_CONTEXT);
  
  while (count_images < numberImagesToTrain)
  {
    labelNum = labelFile->read();
    count_images++;
    
    int imageCategory = labelNum+1;
    ++images_types[labelNum];
              
    for (int i=0; i<LEN; i++)
    {
      pattern[i] = dataFile->read();
    }

    //Prep the image
    int processedImageSize = processImage(pattern);
    
    for (int image_learn = 0; image_learn < NUMBER_ITERATIONS_LEARN_SAME_IMAGE; image_learn++)
    {
      neuronsCommitted = hNN.learn(pattern, processedImageSize, imageCategory);  
    }
    
    Serial.print("Finished training image label :");
    Serial.print(count_images);
    Serial.print("....");
    Serial.print(NUMBER_ITERATIONS_LEARN_SAME_IMAGE);
    Serial.println(" times.");
  }  

  Serial.println("Just learned the following images types");
  for (int i =0; i< NUM_IMAGES_TYPES; i++)
  {
    Serial.print("Image Category:");
    Serial.print(i);
    Serial.print(" Leant : ");
    Serial.print(images_types[i]);
    Serial.println(" images.");
  }
  
  closeFiles();
}

int startNeuronIndex = 1;
int currentNeuronDisplayIndex = startNeuronIndex;

void dumpNNALL()
{
  dumpNN(MAX_NEURONS, true);
}

void printNeuron(int neuron, bool eachNeuron)
{
  hNN.getNeuronsInfo(&neuronSize, &neuronsAvailable, &neuronsCommitted);
  
  if (neuron == 0)
  {
    Serial.print("\nSpecial Neuron ZERO:");
  }
  else
  {
    Serial.print("\nNeuron:"); Serial.print(neuron);
  }
  
  byte model[neuronSize];
  hNN.readNeuron(neuron, &ncr, model, &aif, &cat);
  
  if (eachNeuron)
  {
    Serial.print("\tmodel=");
    for (int k=0; k<LEN; k++) { Serial.print(model[k]); Serial.print(", ");} 
  }
      
  Serial.print("\tncr="); Serial.print(ncr);  
  Serial.print("\taif="); Serial.print(aif);     
  Serial.print("\tcat="); Serial.println(cat);
}

void dumpNN(int num, bool eachNeuron)
{
  if (num > MAX_NEURONS)
    num = MAX_NEURONS;
    
  Serial.print("\nDisplay the committed neurons, committed count = "); Serial.println(neuronsCommitted);
  
  Serial.print("Printing :"); Serial.print(num); Serial.println(" neurons");
    
  for (int i=0; i<num+1; i++)
  {  
    printNeuron(i, eachNeuron);
  }
}

void classifyTrainDataNN()
{
  openTrainingDataFiles();
  classifyNN();
}

void classifyTestDataNN()
{
  openTestingDataFiles();
  classifyNN();
}

void clearImagesArray(int *ar)
{
  for (int i=0; i<NUM_IMAGES_TYPES; i++)
  {
    ar[i] =0;
  }
}

void classifyNN()
{
  int count_images = 0;
  int images_correct[NUM_IMAGES_TYPES];
  int images_incorrect[NUM_IMAGES_TYPES];
  int images_unknown[NUM_IMAGES_TYPES];
  int images_tested[NUM_IMAGES_TYPES];
  
  clearImagesArray(images_unknown);
  clearImagesArray(images_incorrect);
  clearImagesArray(images_correct);
  clearImagesArray(images_tested);
  
  int correct = 0;
  int incorrect = 0;
  int unknown = 0;
  
  if (!labelFile || !dataFile)
  {
    Serial.println("Testing failed - files not opened.");
    closeFiles();
    return;
  }

  if (labelFile->available() == 0)
  {
    Serial.println("Testing network failed - label file not available.");
    closeFiles();
    return;
  }

  if (dataFile->available() == 0)
  {
    Serial.println("Testing network failed - data file not available.");
    closeFiles();
    return;
  }

  int labelNum = 0;
  labelFile->seek(trainLabelSeekPos);
  dataFile->seek(trainDataSeekPos);
  
  Serial.print("Got the correct position in data files. Testing the first :");
  Serial.print(NUM_TO_TEST);
  Serial.println(" images in the data");
  hNN.GCR(NETWORK_CONTEXT);
  unsigned char pattern[LEN];
  
  while (count_images < NUM_TO_TEST)
  {
    Serial.print("Reading Test Image:");
    Serial.println(count_images+1);
    
    labelNum = labelFile->read();
    
    //sets up pattern
    for (int i=0; i<LEN; i++)
    {
      pattern[i] = dataFile->read();
    }

    //Prep the image
    int processedImageSize = processImage(pattern);
    
    int cat = INITIAL_CATEGORY;
    
    Serial.print("About to test image of label type : ");
    Serial.print(labelNum);
    Serial.print(" cat : ");
    Serial.println(labelNum+1);

    hNN.classify(pattern, processedImageSize, &dist, &cat, &nid);
     
    ++images_tested[labelNum];
    
    if (cat!=0x7FFF)
    {
      if (cat == INITIAL_CATEGORY)
      {
        ++images_unknown[labelNum];
        Serial.println("Image category unknown");
      }
      else
      {  
        Serial.print("Image category (label Num) #"); Serial.println(cat-1);      
        if (cat-1 == labelNum)
        {
          Serial.println("CORRECT !! ");
          ++images_correct[labelNum];
        }
        else
        {
          Serial.print("dist #"); Serial.println(dist);
          Serial.print("nid #"); Serial.println(nid);

          Serial.println("INCORRECT !!");
          ++images_incorrect[labelNum];
        }
      }
    
      Serial.print("Finished testing image #");
      count_images++;
      Serial.println(count_images);
    }
  }  

  Serial.println("Ok finished testing");

  Serial.print("Tested ");
  Serial.print(count_images);
  Serial.println(" images.");

  for (int index =0; index<NUM_IMAGES_TYPES; index++)
  {
    Serial.print("For image label ");
    Serial.print(index);
    Serial.print(" we tested ");
    Serial.print(images_tested[index]);
    Serial.print(" and classified ");
    Serial.print(images_correct[index]);
    Serial.print(" correct, and ");
    Serial.print(images_incorrect[index]);
    Serial.print(" incorrect and ");
    Serial.print(images_unknown[index]);
    Serial.println(" unknown images.");
  }
  
  closeFiles();
}

void logUserOption(char g)
{
  COMMANDS_LIST[nextCommand] = g;
  nextCommand++;

  //wrap around
  if (nextCommand == MAX_COMMANDS_HISTORY_LEN)
    nextCommand = 0;
}

void printCommandList()
{
    Serial.print("The following commands were entered by the user.");
    for (int i =0; i< MAX_COMMANDS_HISTORY_LEN; ++i)
    {
      Serial.print(COMMANDS_LIST[i]); Serial.print(" ,");
    }
}

void loop()
{
  Serial.println("\n\nWelcome !! Marcus Neural Network Machine");
  Serial.println("Option");
  Serial.println("------");
  Serial.println("1. Clear NN");
  Serial.println("2. Train NN");
  Serial.println("3. Test NN with training data");
  Serial.println("4. Test NN with test data");
  Serial.println("5. View Neural Network State");
  Serial.println("6. View all neurons");
  Serial.println("7. View committed neurons");
  Serial.println("8. SD Card Info");
  
  Serial.println("0. Toggle NN type : KNN or RBF");
  Serial.println("U. Super test");
  Serial.println("M. Change the Max label to learn");
  Serial.println("P. Print out user commands");
  Serial.println("L. Load the NN");
  Serial.println("S. Save the NN");
  
  Serial.println("\nPlease enter your option : ");
  
  while (!Serial.available()) {}
  
  char option = Serial.read();

  Serial.print("User selected:");
  Serial.println(option);
  Serial.println("\n\n");
  logUserOption(option);
  
  switch (option)
  {
    case 'L':
      Serial.println("Loading knowledge data.");
      restoreNetworkKnowledge();
      break;
    case 'S':
      Serial.println("Saving knowledge data.");
      saveNetworkKnowledge();
      break;
    case 'U':
      Serial.println("Super Test !!!!!");
      
      //clear
      clearNN();
        
      //learn
      trainNN();
      
      //test
      Serial.println("Testing the TEST data !");
      classifyTestDataNN();
      
      dumpNNALL();
            
      Serial.print("FINISHED SUPER TEST !!!!!!!!!");
      break;
      
    case '1':
      Serial.println("Clearing the NN");
      clearNN();
    break;
    case '2':
      Serial.println("Training the NN");
      trainNN();
    break;
    case '3':
      Serial.println("Testing the NN with training data");
      classifyTrainDataNN();
    break;
    case '4':
      Serial.println("Testing the NN with test data");
      classifyTestDataNN();
    break;
    case '5':
      Serial.println("View all Neural Network state.");
      NNInfo();
    break;
    case '6':
      Serial.println("View all neurons.");
      dumpNNALL();
    break;
    case '7':
      Serial.println("Viewing the committed neurons only.");
      dumpNN(neuronsCommitted, false);
    break;
    case '8':
      Serial.println("Check the SD cardN");
      checkSDCard();
    break;
    /*case '9':
      Serial.println("Check board status - BLINK !");
      blink();
    break;*/
    case '0':
      Serial.println("Toggling the NN type");
      toggleNNType();
    break;
    case 'M':
    {
      Serial.println("Enter the labels you'd like to learn up to");      
      while (!Serial.available()) {}
      int k = (char)Serial.read()-48;
      Serial.print("Max image label to learn:");
      Serial.println(MAX_LABEL_NUM_TO_TRAIN);
      MAX_LABEL_NUM_TO_TRAIN = k;
    }
    break;
    case 'P':
     printCommandList();
    break;
    default:
      Serial.println("Invalid Option.");
    return;
  }
}

void saveNetworkKnowledge()
{
  const char *filename = "NeurData.dat";
  SerialFlashFile file;

  uint16_t savedState = CuriePME.beginSaveMode();
  Intel_PMT::neuronData neuronData;
  uint32_t fileSize = 128 * sizeof(neuronData);

  Serial.print( "File Size to save is = ");
  Serial.print( fileSize );
  Serial.print("\n");

  create_if_not_exists( filename, fileSize );
  // Open the file and write test data
  file = SerialFlash.open(filename);
  file.erase();

  if (file) {
    // iterate over the network and save the data.
    while( uint16_t nCount = CuriePME.iterateNeuronsToSave(neuronData)) {
      if( nCount == 0x7FFF)
        break;

      Serial.print("Saving Neuron: ");
      Serial.print(nCount);
      Serial.print("\n");
      uint16_t neuronFields[4];

      neuronFields[0] = neuronData.context;
      neuronFields[1] = neuronData.influence;
      neuronFields[2] = neuronData.minInfluence;
      neuronFields[3] = neuronData.category;

      file.write( (void*) neuronFields, 8);
      file.write( (void*) neuronData.vector, 128 );
    }
  }

  CuriePME.endSaveMode(savedState);
  Serial.print("Knowledge Set Saved. \n");
}


bool create_if_not_exists (const char *filename, uint32_t fileSize) {
  if (!SerialFlash.exists(filename)) {
    Serial.println("Creating file " + String(filename));
    return SerialFlash.createErasable(filename, fileSize);
  }

  Serial.println("File " + String(filename) + " already exists");
  return true;
}


void restoreNetworkKnowledge ()
{
  File *file = NULL;
  const char *filename = "NeurData.dat";
  
  file = new File(SD.open(filename));
  if (!file)
  {
    Serial.print("Failed to open the NN data file:");
    Serial.println(filename);
    file->close();
    return;
  }
  else
  {
    Serial.print("Opened the NN data file:");
    Serial.println(filename);

    if (file->available())
    {
      Serial.println("NN data file available.");
    }
    else
    {
      Serial.println("NN Data file NOT available.");
      file->close();
      return;
    }
  }
  
  int32_t fileNeuronCount = 0;

  uint16_t savedState = CuriePME.beginRestoreMode();
  Intel_PMT::neuronData neuronData;

  // Open the file and write test data
  file = SerialFlash.open(filename);

  if (file) {
    // iterate over the network and save the data.
    while(1) {
      Serial.print("Reading Neuron: ");

      uint16_t neuronFields[4];
      file.read( (void*) neuronFields, 8);
      file.read( (void*) neuronData.vector, 128 );

      neuronData.context = neuronFields[0] ;
      neuronData.influence = neuronFields[1] ;
      neuronData.minInfluence = neuronFields[2] ;
      neuronData.category = neuronFields[3];

      if (neuronFields[0] == 0 || neuronFields[0] > 127)
        break;

      fileNeuronCount++;

      // this part just prints each neuron as it is restored,
      // so you can see what is happening.
      Serial.print(fileNeuronCount);
      Serial.print("\n");

      Serial.print( neuronFields[0] );
      Serial.print( "\t");
      Serial.print( neuronFields[1] );
      Serial.print( "\t");
      Serial.print( neuronFields[2] );
      Serial.print( "\t");
      Serial.print( neuronFields[3] );
      Serial.print( "\t");

      Serial.print( neuronData.vector[0] );
      Serial.print( "\t");
      Serial.print( neuronData.vector[1] );
      Serial.print( "\t");
      Serial.print( neuronData.vector[2] );

      Serial.print( "\n");
      CuriePME.iterateNeuronsToRestore( neuronData );
    }
  }

  CuriePME.endRestoreMode(savedState);
  Serial.print("Knowledge Set Restored. \n");
}


}


