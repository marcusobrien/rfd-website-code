#include <LiquidCrystal.h>
#include <Wire.h>
#include <SoftwareSerial.h>
#include <SPI.h>
#include <SD.h>
#include <MemoryFree.h>

/*This is the old lib contents*/
#pragma once
#include <WString.h>


/* I2C Commands
Object Detection Module
"PINGON", // test for objects
"PINGOF", // dont test for objects
"TELLMM", // tell the mobility module the measurements via i2c
"TELLOF", // dont tell the mobility module the measurements via i2c
"DEBGON", // turn debug on - send to the MDM
"DEBGOF", // turn off debug - dont send anything fown i2c to MDM
"HELPCM"  //List of commands

MOB Debug Module  Commands
SAVESD    - Save to SD card
SENDBT    - Send all across wire via Bluetooth
CLEARS    - Clear the mem
SCRLON    - Scroll mode on
SCRLOF    - Scroll mode off
LIVEON    - send every event up across bluetooth for live monitoring
LIVEOF    - send every event up across bluetooth for live monitoring
LCDONN    - Turn the screen on
LCDOFF    - Turn the screen off (save battery)
HELPCM    - Help commands
SENDI2    - Send out the following comamnd on i2c - the format is COM-SENDI2-ADDRESS-COMMAND ie COM-SENDI2-008-PING
VSTATS    - View stats on both BT and the LCD
*/

//Distance at which to consider an obsticle is in the way - at the lowest speed
//This is used to stop driving and also to light up LEDs
#define SAFE_OBJECT_DISTANCE 35

//This is the distance below which we will consider an invalid metric - ie 0 is not a valid distane, or 1 cm etc
#define MIN_VALID_DISTANCE 5

#define I2CWIRE_DRIVE_UNIT_ADDRESS 1
#define I2CWIRE_OBJECT_DETECT_ADDRESS 2
#define I2CWIRE_101_NN_ADDRESS 69
#define I2CWIRE_MDM_ADDRESS 9
#define I2C_WIRE_DELAY 15

#define NUM_SONICS 9

//#define DEBUG

//EStopped - so we force stopped ie crashed/hit bumper, or the estop was pushed - we need to recover from this
//Stopped - we have stopped moving - speed hit zero
//Moving - Speed > 0
enum CurrentState
{
	EStopped = 0,
	MovingForward = 1,
	MovingBackward = 2,
	DeadTurningLeft = 3,
	DeadTurningRight = 4,
	Stopped = 5
}MAL_CurrentState;

void sendItI2c(const int address, const char *message)
{
	String msg("Sending I2C to:");

	switch (address)
	{
	case I2CWIRE_DRIVE_UNIT_ADDRESS:
#ifdef DEBUG
		Serial.print(msg);
		Serial.println(I2CWIRE_DRIVE_UNIT_ADDRESS);
#endif
		break;
	case I2CWIRE_OBJECT_DETECT_ADDRESS:
#ifdef DEBUG
		Serial.print(msg);
		Serial.println(I2CWIRE_OBJECT_DETECT_ADDRESS);
#endif
		break;
	case I2CWIRE_MDM_ADDRESS:
#ifdef DEBUG
		Serial.print(msg);
		Serial.println(I2CWIRE_MDM_ADDRESS);
#endif
		break;
	default:
#ifdef DEBUG
		Serial.print("I2C Msg Failed.Invalid Add:");
		Serial.println(address);
#endif
		return;
	}

	if (message == NULL || strlen(message) < 1)
	{
#ifdef DEBUG
		Serial.println("I2C Msg Send Fail(len)");
#endif
	}
	else
	{
		Wire.beginTransmission(address);
		Wire.write(message, strlen(message));
		Wire.endTransmission();


#ifdef DEBUG
		Serial.println("I2C Msg:-");
		Serial.println(message);
		Serial.println("I2C Msg Sent");
#endif
	}
}

//Turn this on/off for sending i2c to the MDM unit - dont turn it on if not connected - it freezes the system !
bool USE_MDM = false;

void sendToMDM(String message)
{
	if (!USE_MDM)
		return;

#ifdef DEBUG
	Serial.print("MDM I2C Msg:-");
	Serial.println(message);
#endif

	sendItI2c(I2CWIRE_MDM_ADDRESS, message.c_str());
}

//If you want to debug this debug module !! Then uncomment and use this pre-processor directive.
//All useful output will be sent to the hardware serial monitor 1 (USB) - so conect your SM at the SM baud speed

#define DEBUG

//When busy dont bother updating the UI
bool DONT_UPDATE_UI = false;

//I use this to make sure the latest firmware is downloaded, 
//so after every change I up the version in the string and make sure its correct on the LCD
String FirmwareVersion("MDM V1.11.5");

//When clearing the memory - we want to stop events coming in and filling it up while we are clearing it
bool ClearingMemory = false;

//Live mode means send any messages across bluetooth as they arrive
bool LiveModeON = false;

//This is the token for a command
String CommandToken("COM-");

//Number of memory lines in memory
const int NumberOfMemoryLines = 130;

//Number of stats pages - each time the user presses the stats page it will display the next stat page
const int NUMBER_OF_STAT_PAGES = 3;

int CurrentStatsPage = 0;

/*12c
//white is pin 21 - mega (SCL)
//orange is pin 20 - mega (SDA)


	This is a display module, that I have called the MOB debug Module (MDM). The reason is that I want a specific
	module for reporting errors. This relieves the necessity for debugging on each module etc.

	The format of the messages sent to this MDM are 

	<Sending DEVICE ID> <Message> ;
	
	The max display length is set for the LCD screen, so the length of each message coming in should be up to 256.

	Future Work
	Maybe some sort of filtering for devices

	
  The circuit:
 * LCD RS pin to digital pin 11
 * LCD Enable digital pin 12
 * LCD D4 pin to digital pin 5
 * LCD D5 pin to digital pin 4
 * LCD D6 pin to digital pin 3
 * LCD D7 pin to digital pin 2

 Use pin 20 (SDA) on this MDM Mega Arduino to the SDA pin of the next device for i2c (your master device - sends signals to this device) - for example on the nano use A4
 Use pin 21 (SCL) to SCL on your master device - ie nano would be pin 5 of the next device for i2c

 The i2c wire address of this MDM on the i2c network is number 9.

See the web page on www.RoboticsForDreamers.com for the rest of the pin config.

Written by Marcus O'Brien

See the original 16x2 LCd hello world here.

http://www.arduino.cc/en/Tutorial/LiquidCrystal
 
*/

/* 

Commands from Bluetooth or SM are  - Note that the command must be preceeded by the 4 chars "COM-"

SAVESD		- Save to SD card
SENDBT		- Send all across wire via Bluetooth
CLEARS		- Clear the mem
SCRLON		- Scroll mode on
SCRLOF		- Scroll mode off 
LIVEON		- send every event up across bluetooth for live monitoring
LIVEOF		- send every event up across bluetooth for live monitoring
LCDONN		- Turn the screen on
LCDOFF		- Turn the screen off (save battery)
HELPCM		- Help commands
SENDI2		- Send out the following comamnd on i2c - the format is COM-SENDI2-ADDRESS-COMMAND ie COM-SENDI2-008-PING
VSTATS		- View stats on both BT and the LCD

Version (Not repo version - software version)
1.0 - 1.7	Getting everything working !		
1.8			Adding command mode - receive commands via bluetooth to drive the MDM
1.9			Add i2c command mode - send out commands to all connected devies across i2c
1.10		Added ability to use SM for commands
*/

//Each command must be the same length in characters
static const int COMMAND_LENGTH = 6;

//Length of the device address in the i2c send command
static const int I2C_DEVICE_COMMAND_ADDRESS_LENGTH = 3;

String ListOfCommands [] = 
{
	"SAVESD",
	"SENDBT",
	"CLEARS",
	"SCRLON",
	"SCRLOF",
	"LIVEON",
	"LIVEOF",
	"LCDONN",
	"LCDOFF",
	"HELPCM",
	"SENDI2",
	"VSTATS"
};

//SD card needs this pin for chip select
const int SD_CS_PIN = 10;

//Note this is the SD file name character we will append to the filename write to so it doesnt overwrite
//its gets reset when the board does - so it might overwrite old files if you dont empty it - you have 26 files
//a-z before this happens.
char CurrentFileChar = 'a';

//Bluetooth make sure the BT baud rate is set to 9600 use Serial 1 for it !
//Use pin 19 as Rc on the Arduino so connect it to the BT Tx
//Use pin 18 as Tx on the Arduino so connect it to the BT Rc (via the level shifter !!!)
const int BLUETOOTH_BAUD = 9600;

//LCD Config
//RS Then Enable The Data 
//rs, tx, d4, d5,d6,d7
LiquidCrystal lcd(22, 23, 24, 25, 26, 27);

//Up and down move the display up and down, hold shift to go to top or bottom of memory
//Clear clears the memory, 
//SendBT wil send the memory contents across the bluetooth connection
//and shift with this wil save the memory to sd card - if there is one
//toggle scroll mode on and off - shift with toggle will rotate through the stats pages

//Button Configs 
const int UP = 44; // shift = Top
const int DOWN = 45; // shift = Bottom
const int CLEAR = 46; // shift = nothing
const int TOGGLE_SCROLL = 48; //scroll mode - when a message comes in show line in the LCD 
const int SHIFT = 49; //shift = nothing
const int SENDBT = 47; // shift = save to sd card if there is one

//Traffic light indicator
const int LED_GREEN = 40;
const int LED_RED = 41;
const int LED_AMBER = 42;

//First button on the button panel PCB
const int FirstButton = 44;

//5 normal buttons and one reserved
const int NumberOfButtons = 6;

//LCD Specs
const int LCDNumberOfLines = 4;
const int LCDNumberOfColumns = 20;

//The maximum amount of time to tell the user a message on the LCD in seconds - note a button press will terminate the message
const int LCDMessageTime = 5;

//Time to freeze and display a message from the system - not stats etc
const int LCDMessageTimeFreeze = 2500;

//Globals
int NextAddressToFill = 0;

//When the first line comes in, we want to start displaying at line 0 
int CurrentLineToDisplay = -1;

//Actual memory contents - array of strings -  messages
String displayLines[NumberOfMemoryLines];

//Baud rate for the software serial manager
const int SM_BAUD_RATE = 9600;

//Number of times to flash the LEDS on start up
const int NUM_TIMES_FLASH_LEDS = 3;

//Time to wait after pressing a button, to sto reading the button for a single press as multiple presses
const int POST_BUTTON_DELAY = 350;

//If scroll mode is on, then each new mesasge will be displayed as it comes in
//Otherwise you can move to it manually, it might be a pain when things are constantly
//flashing past - so I will allow it be configured
bool ScrollModeOn = true;

//Bluetooth Constants
//The maxmium message length of a mem address contents
const int MAX_MSG_LENGTH = 256;

//The bluetooth termination character
const char BT_TERMINATION_CHAR = ';';

void initSDCard()
{
	displayMessageOnLCD("Init SD card..");

#ifdef DEBUG
	Serial.print("Init SD card...");
#endif

  if (!SD.begin(SD_CS_PIN, 11, 12, 13))
  {
	  reportError("SD Init Failed!");
  }
  else
  {
#ifdef DEBUG
	  Serial.println("SD Card Initialization done.");
#endif

	  tellUserOnLCD("SD Init Done.");
	  finishedOK();
  }
}

void nextStatPage()
{
	++CurrentStatsPage;
	if (CurrentStatsPage > (NUMBER_OF_STAT_PAGES-1))
		CurrentStatsPage = 0;
}

//Function when doing something
void working()
{
	digitalWrite(LED_AMBER, HIGH);
	digitalWrite(LED_RED, LOW);
	digitalWrite(LED_GREEN, LOW);
}

void finishedOK()
{
	digitalWrite(LED_GREEN, HIGH);
	digitalWrite(LED_AMBER, LOW);
	digitalWrite(LED_RED, LOW);
}

void finishedNOTOK()
{
	digitalWrite(LED_GREEN, LOW);
	digitalWrite(LED_AMBER, LOW);
	digitalWrite(LED_RED, HIGH);
}

//Add a string to memory
void addString(String newMessage, bool fromBT = false)
{
	signed int memLeft = getFreeMemory();
	signed int msgLength = newMessage.length();

#ifdef DEBUG
	Serial.print("AddString:");
	Serial.print(newMessage);
	Serial.print(" Mem Add:");
	Serial.println(NextAddressToFill);
	Serial.println("Free:" + String(memLeft));
	Serial.println("Msg len:" + String(newMessage.length()));
#endif

	if (memLeft < (msgLength + 5))
	{
		reportError("FREE MEM 0");
	}

	//Get rid of any extra spaces/tabs etc
	newMessage.trim();

	//We truncate the line length to be reasonable - 256 is enough
	newMessage.remove(MAX_MSG_LENGTH);

	//if its a command perform it and return
	String BTcommand;
	BTcommand = newMessage;
	BTcommand.toUpperCase();

	if (BTcommand.startsWith(CommandToken))
	{
		int tokenLength = CommandToken.length();

		int tokenAndCommandLength = tokenLength + COMMAND_LENGTH;
		String com = BTcommand.substring(tokenLength, tokenAndCommandLength);

		String extra = "";

		if (BTcommand.length() > tokenAndCommandLength)
		{
			//Add one for the - after the command
			extra = BTcommand.substring(tokenAndCommandLength + 1, BTcommand.length());
		}

		if (fromBT)
			handleCommand(com, extra, Serial1);
		else
			handleCommand(com, extra, Serial);

		return;
	}

	displayLines[NextAddressToFill] = newMessage;

	//Send up to BT and SM if live mode on
	if (LiveModeON)
	{
		Serial1.println(String(NextAddressToFill) + ':' + newMessage);
		Serial.println(String(NextAddressToFill) + ':' + newMessage);
	}

	NextAddressToFill++;

	if (NextAddressToFill >= NumberOfMemoryLines)
	{
#ifdef DEBUG
		Serial.println("Add String wrapped");
#endif
		//Wrapped AROUND! 
		NextAddressToFill = 0;
		if (ScrollModeOn)
			CurrentLineToDisplay = -1;
	}
	else
	{
		//Didnt roll over - so move to the new line if necessary
		if (ScrollModeOn)
		{
			//CurrentLineToDisplay++;
			CurrentLineToDisplay = NextAddressToFill - 1;
			displayMessages();
		}
	}

	if (!ScrollModeOn)
	{
		//If you are looking at line number within the number of LCD lines away from the address you filled - redraw it
		//If you are on the last page of addresses or first page of address - re draw it if the one you filled was > max_add - number of lcd lines
		bool redraw = false;

#ifdef DEBUG
		Serial.print("CurrentLineToDisplay:");
		Serial.print(CurrentLineToDisplay);
		Serial.print("NextAddressToFill:");
		Serial.print(NextAddressToFill);
#endif // DEBUG

		//Youve already incremented the NextAddressToFill so make it LCD lines + 1
		if (CurrentLineToDisplay < 0 && NextAddressToFill < (LCDNumberOfLines + 1))
			redraw = true;

		else if (abs(CurrentLineToDisplay - NextAddressToFill) < (LCDNumberOfLines + 1))
			redraw = true;

		//If currently looking at page 1, and filling address in last page in memeory
		else if (CurrentLineToDisplay < LCDNumberOfLines && NextAddressToFill >(NumberOfMemoryLines - LCDNumberOfLines))
			redraw = true;

		//If looking at last page of memory and filling first page of memory
		else if (CurrentLineToDisplay > (NumberOfMemoryLines - (LCDNumberOfLines + 1)) && (NextAddressToFill < LCDNumberOfLines + 1))
			redraw = true;

		if (redraw)
			displayMessages();
	}
}

//Called once on startup
void setup() 
{
	// Set number of columns and rows
	lcd.begin(LCDNumberOfColumns, LCDNumberOfLines);

	lcd.println("Initializing..");

	//make the LEDs all output only
	pinMode(LED_GREEN, OUTPUT);
	pinMode(LED_AMBER, OUTPUT);
	pinMode(LED_RED, OUTPUT);

	pinMode(19, INPUT_PULLUP);

	//Flash all three LEDS
	for (int i = 0; i < NUM_TIMES_FLASH_LEDS; ++i)
	{
		digitalWrite(LED_AMBER, LOW);
		digitalWrite(LED_RED, LOW);
		digitalWrite(LED_GREEN, HIGH);
		delay(500);
		digitalWrite(LED_RED, LOW);
		digitalWrite(LED_GREEN, LOW);
		digitalWrite(LED_AMBER, HIGH);
		delay(500);
		digitalWrite(LED_GREEN, LOW);
		digitalWrite(LED_AMBER, LOW);
		digitalWrite(LED_RED, HIGH);
		delay(500);
	}

	working();


	Serial.begin(SM_BAUD_RATE);
	Serial.print("MDM program:");
	Serial.println(FirmwareVersion);

	//Set up the buttons - make them high, as they will allow a low when pressed (connected to -ve)
	for (int but = FirstButton; but < FirstButton+NumberOfButtons; but++)
		digitalWrite(but, HIGH);

	//Now the BT module
	Serial1.begin(BLUETOOTH_BAUD);
	delay(500);
	
	initSDCard();


	//Set up some default text
	addString("This is Robot MAL1.");
	addString(FirmwareVersion);
	addString("By Marcus O'Brien");

	//#ifdef DEBUG
	//	//Not for prod - just experiment with how much mem we have to play with for the mega
	//	testMemory();
	//#endif

	// Default welcome message
	displayMessages();

	//For i2c comms (from other Arduinos) we are going to make this a master with bus address 9.
	//This means we can connect 127 more Arduinos 0 - 127 (except address number 9!)
	Wire.begin(I2CWIRE_MDM_ADDRESS);

	// This is the callback function that will be executed when we receive a message for wire 9 - this debug arduino
	Wire.onReceive(receiveEvent);

}

void fillStats(String lines[])
{
	//print out some stats on the LCD, until a key is pressed
	switch (CurrentStatsPage)
	{
	case 0:
	{
		lines[0] = String("MEM:" + String(getFreeMemory()));
		lines[1] = String("Next Add:" + String(NextAddressToFill));
		lines[2] = String("Num Mem:" + String(NumberOfMemoryLines));
		lines[3] = String("i2c Add:" + String(I2CWIRE_MDM_ADDRESS));
	}
	break;
	case 1:
	{
		lines[0] = String("LCD (r,c):(" + String(LCDNumberOfLines) + ',' + String(LCDNumberOfColumns) + ')');
		lines[1] = String("Line To Disp:" + String(CurrentLineToDisplay));
		lines[2] = String("Debug Mode Off");
#ifdef DEBUG
		lines[2] = String("Debug Mode On");
#endif
		lines[3] = String("Live Mode OFF");

		if (LiveModeON)
			lines[3] = String("Live Mode ON");
	}
	break;
	default:
		lines[0] = FirmwareVersion;
		lines[1] = "Current File " + CurrentFileChar;
		break;
	}
	nextStatPage();
}

void displayStats(HardwareSerial &output)
{
	bool scrollWasOn = false;
	if (ScrollModeOn)
	{
		ScrollModeOn = false;
		scrollWasOn = true;
	}
	
	DONT_UPDATE_UI = true;

	String lines[LCDNumberOfLines];
	fillStats(lines);
	displayMessagesOnLCD(lines);

	outputLines(lines,output);
	
	if (scrollWasOn)
		ScrollModeOn = true;
	
	DONT_UPDATE_UI = false;
}

#ifdef DEBUG
void testMemory()
{
	Serial.print("free()");
	Serial.println(getFreeMemory());

	Serial.println("Memory of the Arduino");

	for (int i = 3; i < NumberOfMemoryLines; i++)
	{
		addString('(' + String(i) + ") Testing that this is line");
	}
}
#endif


//This is from i2c only !!
void receiveEvent(int bytes)
{
	if (ClearingMemory)
		return;

	working();

	//When this gets called we had a new message to display. 
	String newString = "";

	if (bytes < 1)
	{
		//Let's protect against reading crap from memory
		newString = "Debug Err";
	}

	//Read from i2C, the new string in the buffer, one char at a time.

	//We must truncate any longer strings than the LCD can display
	//otherwise we get random characters drawn everywhere !
	for (int charNum = 0; charNum<bytes; charNum++)
	{
		//We still need to read until all bytes are consumed to clear the buffer
		//But we only want to fill in our debug message up to the length supported
		char newCharacter = Wire.read();

		//Truncate when we get lcd width
		if (charNum < LCDNumberOfColumns)
		{
		  newString += newCharacter; 
		}
	}

#ifdef DEBUG
	  Serial.print("The rec event " + String(bytes) + " on wire");
	  Serial.print("On rec event-adding ");
	  Serial.println(newString);
#endif

	  //Tell Bluetooth 
	  Serial.print("MDM Got I2C:");
	  Serial.println(newString);

	  //The memory location gets the new string
	  finishedOK();
	  addString(newString);
}

void clearMemoryAndScreen()
{
	ClearingMemory = true;
	working();
	DONT_UPDATE_UI = true;
	bool scrollWasOn = false;
	if (ScrollModeOn)
	{
		ScrollModeOn = false;
		scrollWasOn = true;
	}

#ifdef DEBUG
	Serial.println("Clr Mem and Screen!");
#endif
	displayMessageOnLCD("Clr Mem!");
	for (int i = 0; i < NumberOfMemoryLines; i++)
	{
		displayLines[i] = "";
	}
	CurrentLineToDisplay = -1;
	NextAddressToFill = 0;
	tellUserOnLCD("Memory Clear");

	displayMessages();
	if (scrollWasOn)
	{
		ScrollModeOn = true;
	}

	finishedOK();
	ClearingMemory = false;
	DONT_UPDATE_UI = false;
}

void saveToSD()
{
	working();
	DONT_UPDATE_UI = true;
	bool scrollWasOn = false;
	if (ScrollModeOn)
	{
		ScrollModeOn = false;
		scrollWasOn = true;
	}

	displayMessageOnLCD("Saving to SD");
	String fileName = "";

	//If we get back to the orignal letter we havent found a file that doesnt exist, so any old name will do !
	char originalLetter = CurrentFileChar;

	bool haveValidFilename = false;
	while (haveValidFilename == false)
	{
		fileName = "logfile";
		fileName += CurrentFileChar;
		fileName += ".txt";

		if (SD.exists(fileName.c_str()) == true)
		{

			if (CurrentFileChar == 'z')
				CurrentFileChar = 'a';
			else
				++CurrentFileChar;

			if (CurrentFileChar == originalLetter)
			{
				//we got back to the original letter ! Sono files can be made without overwriting
				//we dont want an infinite loop
				fileName = "FNERROR.txt";
				haveValidFilename = true;
			}
		}
		else
			haveValidFilename = true;
	}

#ifdef DEBUG
	Serial.print("Try and write to:" + fileName);
#endif

	//SD card file - only one can be open !
	File myFile;

	myFile = SD.open(fileName.c_str(), FILE_WRITE);

	//If the file opened then we can write to it
	if (myFile) 
	{
#ifdef DEBUG
		Serial.println("Writing to file ");
#endif
		myFile.println("File write to name:" + fileName);

		int currentStatPage = CurrentStatsPage;
		CurrentStatsPage = 0;

		for (int i = 0; i < NUMBER_OF_STAT_PAGES; ++i)
		{
			String lines[LCDNumberOfLines];
			fillStats(lines);
			for (int line = 0; line < LCDNumberOfLines; ++line)
				Serial1.println(lines[line]);
		}

		CurrentStatsPage = currentStatPage;

		myFile.println("Current line number is : " + String(NextAddressToFill));

		for (int row = 0; row < NumberOfMemoryLines; row++)
		{
			myFile.print(row);
			myFile.println(":" + displayLines[row]);
		}

		//Close the file
		myFile.close();

#ifdef DEBUG
		Serial.println("Done");
#endif
		tellUserOnLCD("Saved SD");
		finishedOK();
	} 
	else 
	{
		reportError("SD write failed.");
	}
	if (scrollWasOn)
	{
		ScrollModeOn = true;
	}
	DONT_UPDATE_UI = false;
}

void sendViaBT()
{
	DONT_UPDATE_UI = true;
	working();
	bool scrollWasOn = false;
	if (ScrollModeOn)
	{
		ScrollModeOn = false;
		scrollWasOn = true;
	}

	//Should have a test to make sure the BT is connected
	displayMessageOnLCD("Mem dump via BT");
	Serial1.println("MDM Out Start");	
	Serial1.println("Mem Add:" + String(NextAddressToFill-1));

	int currentStatPage = CurrentStatsPage;
	CurrentStatsPage = 0;

	for (int i = 0; i < NUMBER_OF_STAT_PAGES; ++i)
	{
		String lines[LCDNumberOfLines];
		fillStats(lines);
		for (int line = 0; line < LCDNumberOfLines; ++line)
			Serial1.println(lines[line]);
	}
	
	CurrentStatsPage = currentStatPage;

	for (int row = 0; row < NumberOfMemoryLines; row++)
	{
		Serial1.print(row);
		String message = displayLines[row];
		if (message.length() < 1)
			message = "EMPTY";
		Serial1.println(" - " + message);
		Serial1.println("");
	}

	Serial1.println("MDM Output Finished");
	tellUserOnLCD("Success - Mem BT");
	if (scrollWasOn)
	{
		ScrollModeOn = true;
	}

	finishedOK();
	DONT_UPDATE_UI = false;
}

//Tests to see if any button is pressed
bool isButtonPressed()
{
	for (int but = FirstButton; but < FirstButton+NumberOfButtons; ++but)
	{
		if (digitalRead(but) == LOW)
			return true;
	}
	return false;
}

void waitOrButtonPress(int maxSecondsTimeToWait)
{
	//Lets make sure all buttons are off first, then one is pressed again, this will prevent
	//the screen vanishing if they have their finger held on the button
	bool scrollWasOn = false;
	if (ScrollModeOn)
	{
		ScrollModeOn = false;
		scrollWasOn = true;
	}

	bool No_Buttons_Pressed = false;

	for (int i = maxSecondsTimeToWait*20; i > 0; --i)
	{
		delay(50);
		if (isButtonPressed())
		{
			if (No_Buttons_Pressed == true)
			{
				//Wait here until they take their finger off the button, otherwise the loop will pick the button up
				//and do the actin - could be clear the mem !
				while (isButtonPressed());
				if (scrollWasOn)
				{
					ScrollModeOn = true;
				}

				return;
			}
		}
		else
		{
			//The took their finger off, so now we will let the screen
			//clear if they press another one
			No_Buttons_Pressed = true;
		}
	}
	if (scrollWasOn)
	{
		ScrollModeOn = true;
	}
}

//This func clears the screen then displays the message for X seconds
void tellUserOnLCD(String message)
{
	displayMessageOnLCD(message);
	Serial1.println(message);
	Serial.println(message);
	waitOrButtonPress(LCDMessageTime);
	displayMessages();
}

void outputLines(String lines[], HardwareSerial &output)
{
	for (int i = 0; i < LCDNumberOfLines; ++i)
	{
		output.println(lines[i]);
	}

}

void displayMessagesOnLCD(String lines[])
{
#ifdef DEBUG
	for (int i = 0; i < LCDNumberOfLines; ++i)
	{
		Serial.println("Msg stat to display-" + lines[i]);
	}
#endif
	bool Button_released = false;

	for (int i = LCDMessageTime * 20; i > 0; --i)
	{
		display4Lines(lines);

		delay(50);
		if (!isButtonPressed())
		{
			Button_released = true;
		}

		if (Button_released == true)
		{
			while (isButtonPressed())
			{
				display4Lines(lines);
			}
			break;
		}
	}
	displayMessages();
}

void display4Lines(String lines[])
{
	lcd.clear();
	for (int i = 0; i < LCDNumberOfLines; ++i)
	{
		lcd.setCursor(0, i);
		String display = lines[i].substring(0, LCDNumberOfColumns);
		lcd.print(display);
	}
}

void displayMessageOnLCD(String message)
{
	lcd.clear();
	lcd.setCursor(0, 0);
	String display = message.substring(0, LCDNumberOfColumns);
	lcd.print(display);
	delay(LCDMessageTimeFreeze);
}

void toggleScroll()
{
	if (ScrollModeOn)
	{
		ScrollModeOn = false;
		tellUserOnLCD("Scroll OFF");
	}
	else
	{
		ScrollModeOn = true;
		tellUserOnLCD("Scroll ON");
	}
}

String PreviousBTMessage = "";

void loop()
{
	while (Serial1.available())
	{
		//Test to see if BT has a string to get, if add it to the mem
		char message[MAX_MSG_LENGTH];
		size_t msgLength = Serial1.readBytesUntil(BT_TERMINATION_CHAR, message, MAX_MSG_LENGTH);

		delay(50);

#ifdef DEBUG
		Serial.println("msg len from bt:" + String((int)msgLength));
		Serial.println("msg from bt:" + String((int)message));
#endif

		//Put on a null for end of string
		message[msgLength] = char(0);

		String readString(message);

		if (readString.length() > 0)
		{
#ifdef DEBUG
			Serial.println("From BT,adding to Mem:" + readString + " len:" + String(readString.length()));
#endif
			if (readString.compareTo(PreviousBTMessage) == 0)
			{
				//same message as before, so ignore it 
				PreviousBTMessage = "";
				return;
			}
			else
			{
				Serial1.println("Got Msg Via BT:" + String(message));
				addString(readString, true);
				PreviousBTMessage = readString;
			}
		}
#ifdef DEBUG
		else
			Serial.println("Msg from BT empty");
#endif
	}

	//Now see if there is anything on the serial monitor wire
	String readString("");

	while (Serial.available())
	{
		if (Serial.available() > 0)
		{
			char c = Serial.read();
			readString.concat(c);
#ifdef DEBUG
			Serial.println("New char SM:");
			Serial.println(c);
			Serial.println("Sofar:");
			Serial.println(readString);
#endif
		}
	}

	if (readString.length() > 0)
	{
		Serial.print("Msg from SM:");
		Serial.println(readString);

		addString(readString);
	}

	handleButtons();
}

void handleButtons()
{
	//We need to make sure the bounce on the buttons is sorted - eg. holding down a button for <200ms is not pressing it twice !
	//Wait for time if the user pressed something - so we dont come flying in here and read that they pressed it agin when they are 
	//still pressing it the first time.
	
	//Pressing up takes user back to 0
	//Down takes them to the top memory address

	//Shift is not presently pressed
	if (digitalRead(SHIFT) == HIGH)
	{
		if (digitalRead(UP) == LOW)
		{
#ifdef DEBUG
			Serial.println("Psd UP-CLD:" + String(CurrentLineToDisplay));
#endif 

			//If we are on the first page, cant go up any higher
			//If we are not on the first page, move the current line to display up a page
			if (CurrentLineToDisplay < LCDNumberOfLines)
			{
				CurrentLineToDisplay = 0;
			}
			else
			{
				CurrentLineToDisplay = CurrentLineToDisplay - LCDNumberOfLines;
			}
			displayMessages();
			delay(POST_BUTTON_DELAY);
		}
		else if (digitalRead(DOWN) == LOW)
		{
#ifdef DEBUG
			Serial.println("Psd DWN-CLD:" + String(CurrentLineToDisplay));
#endif 

			//We are on the last page so dont move the currenty line
			if (CurrentLineToDisplay >  NumberOfMemoryLines - LCDNumberOfLines)
			{
				CurrentLineToDisplay = NumberOfMemoryLines - LCDNumberOfLines + 1;
			}
			else
			{
				CurrentLineToDisplay = CurrentLineToDisplay + LCDNumberOfLines;
			}
			displayMessages();
			delay(POST_BUTTON_DELAY);
		}
		else if (digitalRead(CLEAR) == LOW)
		{
#ifdef DEBUG
			Serial.println("Prd Clr Mem");
#endif // DEBUG

			clearMemoryAndScreen();
		}
		else if (digitalRead(SENDBT) == LOW)
		{
#ifdef DEBUG
			Serial.println("Psd snd BT");
#endif // DEBUG

			sendViaBT();
		}
		else if (digitalRead(TOGGLE_SCROLL) == LOW)
		{
#ifdef DEBUG
			Serial.println("Psd tog scroll:");
			if (ScrollModeOn)
				Serial.println("off");
			else
				Serial.println("on");
#endif // DEBUG

			toggleScroll();
		}
	}
	else //shift is pressed down
	{
		if (digitalRead(UP) == LOW)
		{
			//Display the lines from 0 to number of LCD lines -1
			CurrentLineToDisplay = 0;
			displayMessages();
			delay(POST_BUTTON_DELAY);
		}
		else if (digitalRead(DOWN) == LOW)
		{
			CurrentLineToDisplay = NumberOfMemoryLines;
			displayMessages();
			delay(POST_BUTTON_DELAY);
		}
		else if (digitalRead(CLEAR) == LOW)
		{
			//Nothing for this button with shift at the moment	
		}
		else if (digitalRead(SENDBT) == LOW)
		{
#ifdef DEBUG
			Serial.println("Psd save SD");
#endif
			saveToSD();
		}
		else if (digitalRead(TOGGLE_SCROLL) == LOW)
		{
			//Print out device stats ie free memory, current mem size, device num, currentl mem line
#ifdef DEBUG
			Serial.println("Psd Stats");
#endif
			displayStats(Serial);
		}
	}
#ifdef DEBUG
	for (int but = FirstButton; but < FirstButton+NumberOfButtons; but++)
	{
		if (digitalRead(but) == LOW)
		{
			Serial.println("Posd Psd-CLD:" + String(CurrentLineToDisplay));
			Serial.println("Psd But:" + String(but));
		}
	}
#endif
}

void reportError(String err)
{
	String message = "ERR ";
	message.concat(err);

#ifdef DEBUG
	Serial.print("ER Msg:");
	Serial.println(message);
#endif
	tellUserOnLCD(message.substring(0,LCDNumberOfColumns));
	finishedNOTOK();
}


void handleCommand(String command, String extraData, HardwareSerial &output)
{
	command.trim();

	//Tell BT
	Serial1.println("MDM Got CMD:");
	Serial1.print(command);
	Serial1.print(" - ");
	Serial1.println(extraData);


#ifdef DEBUG
	Serial.println("CMD-Len:" + String(command.length()) + " cmd:" + command);

	if (extraData.length() > 0)
	{
		Serial.println("Ex Data:" + extraData);
	}
	else
	{
		Serial.println("NO Extra");
	}
#endif

	if (command.length() != COMMAND_LENGTH)
	{
		commandErrror("Invalid CMD len", output);
		return;
	}
	
	if (extraData.length() > 0)
	{
		extraData.trim();
		extraData.toUpperCase();
	}

	command.toUpperCase();

	output.println("Command : " + command);

	if (command.compareTo("SAVESD") == 0)
	{
		output.println("Sv Mem SD");
		saveToSD();
	}
	else if (command.compareTo("SENDBT") == 0)
	{
		output.println("Snd Mem BT");
		sendViaBT();
	}
	else if (command.compareTo("CLEARS") == 0)
	{
		output.println("Clr mem");
		clearMemoryAndScreen();
	}
	else if (command.compareTo("VSTATS") == 0)
	{
		displayStats(output);
	}
	else if (command.compareTo("SCRLON") == 0)
	{
		ScrollModeOn = true;
		output.println("Scroll is ON");
		tellUserOnLCD("Scroll is ON");
	}
	else if (command.compareTo("SCRLOF") == 0)
	{
		ScrollModeOn = false;
		output.println("Scroll is OFF");
		tellUserOnLCD("Scroll is OFF");
	}
	else if (command.compareTo("LIVEON") == 0)
	{
		LiveModeON = true;
		output.println("LiveMode ON");
		tellUserOnLCD("LiveMode ON");
	}
	else if (command.compareTo("LIVEOF") == 0)
	{
		LiveModeON = false;
		output.println("LiveMode OFF");
		tellUserOnLCD("LiveMode OFF");
	}
	else if (command.compareTo("LCDONN") == 0)
	{
		output.println("LCD ON");
		lcd.display();
	}
	else if (command.compareTo("LCDOFF") == 0)
	{
		output.println("LCD OFF");
		lcd.noDisplay();
	}
	else if (command.compareTo("HELPCM") == 0)
	{
		supportedCommands(Serial1);
		supportedCommands(Serial);
	}
	else if (command.compareTo("SENDI2") == 0)
	{
		//The format is COM - SENDI2 - ADDRESS - COMMAND ie COM - SENDI2 - 008-PING

		String target = extraData.substring(0, I2C_DEVICE_COMMAND_ADDRESS_LENGTH);

		int targetId = (int)target.toInt();

#ifdef DEBUG
		Serial.println("I2C cmd Add:" + target);
		Serial.print("I2C cmd Add as int:");
		Serial.println(targetId);
#endif
		output.print("Snd the cmd:" + command);
		output.print(" Add: " + target);
		output.println(" i2c");
		
		sendItI2c(targetId, extraData.substring(I2C_DEVICE_COMMAND_ADDRESS_LENGTH + 1, extraData.length()).c_str());
	}
	else
	{

#ifdef DEBUG
		Serial.println("Inv Cmd from BT");
#endif
		output.println("Inv Cmd");
		commandErrror("Ukn Cmd", output);
	}
}

void commandErrror(String message, HardwareSerial &output)
{
	reportError(message);
	supportedCommands(output);

#ifdef DEBUG
	supportedCommands(Serial);
#endif
}

void supportedCommands(HardwareSerial &serial)
{
	serial.println("To send an i2c command - COM-SENDI2-ADDRESS-COMMAND");
	serial.println("eg to turn on the ping mode for the object detector use COM-SENDI2-001-PINGON");
	serial.println("When sending BT commands the BT windows client sometimes sends the message twice.");
	serial.println("Stop duplicate messages from the BT device, end with ;");
	serial.println("Commands:");
	
	int numberOfCommands = sizeof(ListOfCommands) / COMMAND_LENGTH;

	for (int i = 0; i < numberOfCommands; i++)
	{
		serial.println(ListOfCommands[i]);
	}
}

void displayMessages()
{
	if (DONT_UPDATE_UI)
		return;

  int LCDrowNumber = 0;
  lcd.clear();  
  
  // Set the cursor to column 0, line 0 to start with
  // Line 1 is the second row, since row counting begins with 0
  // Index is column, row
  lcd.setCursor(0, LCDrowNumber);

  int startdisplayRow = CurrentLineToDisplay - LCDNumberOfLines + 1;

  if (CurrentLineToDisplay == -1)
  {
	  //start with empty, or display from start
	  startdisplayRow = 0;
  }
  else
  {
	  if (CurrentLineToDisplay >= (NumberOfMemoryLines - LCDNumberOfLines))
	  {
		  startdisplayRow = NumberOfMemoryLines - LCDNumberOfLines;
	  }
	  else
	  {
		  startdisplayRow = CurrentLineToDisplay;
	  }
  }

  //If in scroll mode then start at the line just filled - lcd lines
  if (ScrollModeOn)
  {
	  //If we are on the first page, always start at 0 and fill in
	  if (NextAddressToFill < LCDNumberOfLines)
		  startdisplayRow = 0;
	  else
		  startdisplayRow = NextAddressToFill - LCDNumberOfLines;
  }

  int endLine = startdisplayRow + LCDNumberOfLines;

  for (int row = startdisplayRow; row < endLine; row++)
  {
    LCDPrintMemoryLine(row);
    LCDrowNumber++;
    lcd.setCursor(0, LCDrowNumber);
  }
}

void LCDPrintMemoryLine(int location)
{
#ifdef DEBUG
	Serial.println('(' + String(location) + ") : " + displayLines[location]);
#endif

	if (location < NumberOfMemoryLines)
	{		 
		//If in  debug display the memory address then the device address then the string
		//if not in debug just the device address and the message

		//Put on the mem address
		String message = String(location) + displayLines[location];
		
		//Only display the max num chars the lcd can display 
		String display = message.substring(0, LCDNumberOfColumns);
		lcd.print(display);

		//Dont display the mem add - print raw message
		//lcd.print(displayLines[location]);
  }
  else
  {
    lcd.print("Er Add:" + String(location));
  }
}
