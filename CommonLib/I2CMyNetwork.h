#pragma once
#include <WString.h>


/* I2C Commands 
 Object Detection Module
  "PINGON", // test for objects
  "PINGOF", // dont test for objects
  "TELLMM", // tell the mobility module the measurements via i2c
  "TELLOF", // dont tell the mobility module the measurements via i2c
  "DEBGON", // turn debug on - send to the MDM
  "DEBGOF", // turn off debug - dont send anything fown i2c to MDM
  "HELPCM"  //List of commands

  MOB Debug Module  Commands
  SAVESD    - Save to SD card
  SENDBT    - Send all across wire via Bluetooth
  CLEARS    - Clear the mem
  SCRLON    - Scroll mode on
  SCRLOF    - Scroll mode off 
  LIVEON    - send every event up across bluetooth for live monitoring
  LIVEOF    - send every event up across bluetooth for live monitoring
  LCDONN    - Turn the screen on
  LCDOFF    - Turn the screen off (save battery)
  HELPCM    - Help commands
  SENDI2    - Send out the following comamnd on i2c - the format is COM-SENDI2-ADDRESS-COMMAND ie COM-SENDI2-008-PING
  VSTATS    - View stats on both BT and the LCD
 */
 
//Distance at which to consider an obsticle is in the way - at the lowest speed
//This is used to stop driving and also to light up LEDs
#define SAFE_OBJECT_DISTANCE 35

//This is the distance below which we will consider an invalid metric - ie 0 is not a valid distane, or 1 cm etc
#define MIN_VALID_DISTANCE 5

#define I2CWIRE_DRIVE_UNIT_ADDRESS 1
#define I2CWIRE_OBJECT_DETECT_ADDRESS 2
#define I2CWIRE_101_NN_ADDRESS 69
#define I2CWIRE_MDM_ADDRESS 9
#define I2C_WIRE_DELAY 15

#define NUM_SONICS 9

//#define DEBUG

//EStopped - so we force stopped ie crashed/hit bumper, or the estop was pushed - we need to recover from this
//Stopped - we have stopped moving - speed hit zero
//Moving - Speed > 0
enum CurrentState
{
	EStopped = 0,
	MovingForward = 1,
	MovingBackward = 2,
	DeadTurningLeft = 3,
	DeadTurningRight = 4,
	Stopped = 5
}MAL_CurrentState;

void sendItI2c(const int address, const char *message)
{
	String msg("Sending I2C to:");

	switch (address)
	{
		case I2CWIRE_DRIVE_UNIT_ADDRESS:
#ifdef DEBUG
			Serial.print(msg);
			Serial.println(I2CWIRE_DRIVE_UNIT_ADDRESS);
#endif
			break;
		case I2CWIRE_OBJECT_DETECT_ADDRESS:
#ifdef DEBUG
			Serial.print(msg);
			Serial.println(I2CWIRE_OBJECT_DETECT_ADDRESS);
#endif
			break;
		case I2CWIRE_MDM_ADDRESS:
#ifdef DEBUG
			Serial.print(msg);
			Serial.println(I2CWIRE_MDM_ADDRESS);
#endif
			break;
		default:
#ifdef DEBUG
			Serial.print("I2C Msg Failed.Invalid Add:");
			Serial.println(address);
#endif
			return;
	}
	
	if (message == NULL || strlen(message) < 1)
	{
#ifdef DEBUG
		Serial.println("I2C Msg Send Fail(len)");
#endif
	}
	else
	{
		Wire.beginTransmission(address);
		Wire.write(message, strlen(message));
		Wire.endTransmission();


#ifdef DEBUG
		Serial.println("I2C Msg:-");
		Serial.println(message);
		Serial.println("I2C Msg Sent");
#endif
	}
}

//Turn this on/off for sending i2c to the MDM unit - dont turn it on if not connected - it freezes the system !
bool USE_MDM = false;

void sendToMDM(String message)
{
	if (!USE_MDM)
		return;

#ifdef DEBUG
	Serial.print("MDM I2C Msg:-");
	Serial.println(message);
#endif

	sendItI2c(I2CWIRE_MDM_ADDRESS, message.c_str());
}
