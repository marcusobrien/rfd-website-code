#include <Servo.h>

#define NUM_SERVOS 6

#define CLAW 0 //Index of serv for claw
#define HAND 1 //Index of serv for hand

int startPositions[NUM_SERVOS] = {35,  0,   40,  15,  20,  0};
int maxPositions[NUM_SERVOS] =   {110, 180, 180, 160, 90, 180};

String version = "1.20 PROD";

Servo myservo[NUM_SERVOS]; 

bool testDirectionUp = true;
int testDelayMs = 250;
int testFlipDelayMs = 1000;

void test(int servo)
{
  Serial.println("Testing servo - 0=claw, 1=hand 2=wrist etc #");
  Serial.println(servo);
  
  int pos = startPositions[servo];
  bool go = true;
  
  while (go)
  {
    if (pos <startPositions[servo])
    {
      testDirectionUp = true;
      Serial.println("Flipped to going up");
      delay(testFlipDelayMs);
    }
    else if (pos > maxPositions[servo])
    {
      Serial.println("Flipped to going down");
      testDirectionUp = false;
      delay(testFlipDelayMs);
    }
      
    if (testDirectionUp)
    {
      pos++;
    }
    else
    {
     pos--;
    }
    Serial.print("Now at pos :");
    Serial.println(pos);
    
    myservo[servo].write(pos);  
    delay(testDelayMs);
  }
}

void setupPins()
{
  for (int servo = 0; servo < NUM_SERVOS; ++servo)
  {
    pinMode(servo, OUTPUT);
    myservo[servo].attach(servo+2);
    myservo[servo].write(startPositions[servo]);  
  }
}

void setup() 
{  
  Serial.begin(9600);  
  Serial.println("--- Start Serial Monitor SEND_RCVE ---");
  Serial.print("Version:");
  Serial.println(version);
  setupPins();
}

static const unsigned int timeDelay = 250;

static bool ClawDirectionUp = true;
static const int ClawMax = 105;
static const int ClawMin = 25;
static unsigned int ClawInc = ClawMin;

void ClawLoop() 
{    
    if (ClawDirectionUp  == true)
    {
        ++ClawInc;
        if (ClawInc > ClawMax)
        {
          Serial.println("Flipped to downwards - hit ClawMax");
          ClawDirectionUp  = false;
          return;
        }
    }
    else
    {
        --ClawInc;
        if (ClawInc < ClawMin)
        {
          Serial.println("Flipped to upwards");
          ClawDirectionUp  = true;
          return;
        }
    }
    Serial.print("Setting signal to:");
    Serial.println(ClawInc);
    myservo[CLAW].write(ClawInc);
    delay(timeDelay);
}

static bool HandDirectionUp = true;
static const int HandMax = 175;
static const int HandMin = 10;
static unsigned int HandInc = HandMin;

void HandLoop() 
{    
    if (HandDirectionUp  == true)
    {
        ++HandInc;
        if (HandInc > HandMax)
        {
          Serial.println("Hand Flipped to downwards - hit HandMax");
          HandDirectionUp  = false;
          return;
        }
    }
    else
    {
        --HandInc;
        if (HandInc < HandMin)
        {
          Serial.println("Hand Flipped to upwards - hit HandMin");
          HandDirectionUp  = true;
          return;
        }
    }
    Serial.print("Hand Setting signal to:");
    Serial.println(HandInc);
    myservo[HAND].write(HandInc);
    delay(timeDelay);
}

void loop() 
{    
  Serial.println("In loop doing nothing");
    //ClawLoop();    
    //HandLoop();    
}
