#include <Servo.h>

/*
 * This embedded Aruidno program is for controlling a dual USB camera system.
 * The controller adjusts 2 input signals per camera, these signals control 2
 * servos for rotation (yaw) and pitch (up and down)
 * The read signals from each camera are from a IR sensor.
 */
String version = "6.5 DEBUG";
 
#define NUM_SERVOS 4

#define LEFT_ROTATE 3 //Index of Left rotation - looking at eye from front
#define LEFT_TILT 9 //Index of Left tilt - looking at eye from front

#define RIGHT_ROTATE 10 //Index of right rotation - looking at eye from front
#define RIGHT_TILT 11 //Index of right tilt - looking at eye from front

#define PIR_LEFT_EYE 7 //As you look at the eyes the left one
#define PIR_RIGHT_EYE 8 //As you look at the eyes the right one

#define LED_LEFT_EYE 12 //As you look at the eyes the left one
#define LED_RIGHT_EYE 13 //As you look at the eyes the right one

//Servo Index
#define LRotationIndex 0
#define LElavationIndex 1
#define RRotationIndex 2
#define RElavationIndex 3

#define BAUDRATE 9600 //UART rate

Servo myservo[NUM_SERVOS]; 

static const int SECOND = 1000;
static const int LED_INCREMENT = SECOND;

int LED_DELAY_VALUE = 4 * SECOND;

enum VisionState
{
  idle, 
  test,
  search
};

VisionState currentState = idle;

bool TEST_SERVOS = false;
bool TEST_LEDS = false;
bool TEST_LEFT_PIRS = false;
bool TEST_RIGHT_PIRS = false;

unsigned int eyeElevationL = 0;
unsigned int eyeElevationR = 0;
unsigned int eyeRotationL = 0;
unsigned int eyeRotationR = 0;

//rotation, elevation left then right
unsigned int startPositions[] = {45,  95,   45,  95};
unsigned int maxPositions[] =   {170, 150, 170, 150};

bool testDirectionUpL = true;
bool testDirectionUpR = true;
bool testDirectionRightL = true;
bool testDirectionRightR = true;

unsigned const int testDelayMs = 20;
unsigned const int testFlipDelayMs = 100;
unsigned const int startUpDelay = 200;

void changeState(VisionState newState)
{
  Serial.print("Changing state from : ");
  Serial.print(currentState);
  Serial.print(" to ");
  Serial.println(newState);
  
  currentState = newState;  
}

void setupServoPins(bool reset)
{
      pinMode(LEFT_ROTATE, OUTPUT);
      pinMode(LEFT_TILT, OUTPUT);
      pinMode(RIGHT_ROTATE, OUTPUT);
      pinMode(RIGHT_TILT, OUTPUT);
      
      myservo[LRotationIndex].attach(LEFT_ROTATE);
      myservo[LElavationIndex].attach(LEFT_TILT);
      myservo[RRotationIndex].attach(RIGHT_ROTATE);
      myservo[RElavationIndex].attach(RIGHT_TILT);
      
      if (reset)
      {
        Serial.print("Resetting all back to start servo");
        myservo[LRotationIndex].write(startPositions[LRotationIndex]);  
        myservo[LElavationIndex].write(startPositions[LElavationIndex]);  
        myservo[RRotationIndex].write(startPositions[RRotationIndex]);  
        myservo[RElavationIndex].write(startPositions[RElavationIndex]);  
      }
}

void setupPins()
{
  setupServoPins(true);
  
  Serial.print("Pin #");
  Serial.print(PIR_RIGHT_EYE);
  Serial.println(" to in PIR right eye");
  pinMode(PIR_RIGHT_EYE, INPUT);
  
  Serial.print("Pin #");
  Serial.print(PIR_LEFT_EYE);
  Serial.println(" to in PIR left eye");
  pinMode(PIR_LEFT_EYE, INPUT);

  Serial.print("Pin #");
  Serial.print(LED_RIGHT_EYE);
  Serial.println(" out LED Right");
  pinMode(LED_RIGHT_EYE, OUTPUT);
  
  Serial.print("Pin #");
  Serial.print(LED_LEFT_EYE);
  Serial.println(" out LED Left");
  pinMode(LED_LEFT_EYE, OUTPUT);

  delay(startUpDelay);
}

bool moveLeftEye = true;
bool moveRightEye = true;

void reset()
{
  testDirectionUpL = true;
  testDirectionRightL = true;
  testDirectionUpR = true;
  testDirectionRightR = true;
  
  changeState(idle);
  resetServos();
  
  TEST_SERVOS = false;
  TEST_LEDS = false;
  TEST_RIGHT_PIRS = false;
  TEST_LEFT_PIRS = false;

  moveLeftEye = false;
  moveRightEye = false;
}

void resetServos()
{
  eyeRotationL = startPositions[LRotationIndex];
  eyeRotationR = startPositions[RRotationIndex];
  eyeElevationL = startPositions[LElavationIndex];
  eyeElevationR = startPositions[RElavationIndex];

  for (unsigned int servo = LRotationIndex; servo < NUM_SERVOS; ++servo)
  {
    Serial.print("Set servo #");
    Serial.print(servo);
    Serial.print(" start pos : ");
    Serial.println(startPositions[servo]);  
    myservo[servo].write(startPositions[servo]);  
  }
}


void scanArea()
{
  int PIRRight = digitalRead(PIR_RIGHT_EYE);
  int PIRLeft = digitalRead(PIR_LEFT_EYE);

  digitalWrite(LED_RIGHT_EYE,PIRRight);            
  digitalWrite(LED_LEFT_EYE,PIRLeft);  

  //Not left target
  if (PIRLeft != HIGH)
  {
    if (moveLeftEye)
    {
        if (testDirectionUpL)
          eyeElevationL++;
        else
          eyeElevationL--;

        if (testDirectionRightL)
          eyeRotationL++;
        else
          eyeRotationL--;

      setServoSignal(LRotationIndex, eyeRotationL);    
      setServoSignal(LElavationIndex, eyeElevationL);    
    }
  }
  else
  {
    Serial.println("Left eye has target");
    moveLeftEye = false;
  }

  if (PIRRight != HIGH)
  {
    if (moveRightEye)
    {
      if (testDirectionUpR)
        eyeElevationR++;
      else
        eyeElevationR--;
      if (testDirectionRightR)
        eyeRotationR++;
      else
        eyeRotationR--;

      setServoSignal(RRotationIndex, eyeRotationR);    
      setServoSignal(RElavationIndex, eyeElevationR);    
    }
  }
  else
  {
    Serial.println("Right eye has target");
    moveLeftEye = false;
  }

  if (eyeElevationL < startPositions[LElavationIndex])
  {
    eyeElevationL = startPositions[LElavationIndex];
    testDirectionUpL = true;
  }
  else if (eyeElevationL > maxPositions[LElavationIndex])
  {
    eyeElevationL = maxPositions[LElavationIndex];
    testDirectionUpL = false;
  }
  
  if (eyeElevationR < startPositions[RElavationIndex])
  {
    eyeElevationR = startPositions[RElavationIndex];
    testDirectionUpR = true;
  }
  else if (eyeElevationR > maxPositions[RElavationIndex])
  {
    eyeElevationR = maxPositions[RElavationIndex];
    testDirectionUpR = false;
  }

  if (eyeRotationL < startPositions[LRotationIndex])
  {
    eyeRotationL = startPositions[LRotationIndex];
    testDirectionRightL = true;
  }
  else if (eyeRotationL > maxPositions[LRotationIndex])
  {
    eyeRotationL = maxPositions[LRotationIndex];
    testDirectionRightL = false;
  }

  if (eyeRotationR < startPositions[RRotationIndex])
  {
    eyeRotationR = startPositions[RRotationIndex];
    testDirectionRightR = true;
  }
  else if (eyeRotationR > maxPositions[RRotationIndex])
  {
    eyeRotationR = maxPositions[RRotationIndex];
    testDirectionRightR = false;
  }
}

static const unsigned int testStart = 45;
static const unsigned int testMax = 140;

bool testingPositive = true;

unsigned int testSignal = testStart;
unsigned int moveServo = 0;

void resetTest()
{
  testingPositive = true;
  testSignal = testStart;
  moveServo = 0;  
  reset();
}

void testSystem()
{
  if (TEST_SERVOS)
  {
    if (testingPositive)
      testSignal++;
    else
      testSignal--;

    if (testSignal > testMax)
      testingPositive = false;
    else if (testSignal < testStart) 
      testingPositive = true;
    
    setServoSignal(moveServo, testSignal);
    
    delay(testDelayMs);
    
    moveServo++;
    if (moveServo > 3)
      moveServo=0;
  }  
  if (TEST_LEDS)
  {
    digitalWrite(LED_RIGHT_EYE,HIGH);  
    digitalWrite(LED_LEFT_EYE,HIGH);  
    delay(LED_DELAY_VALUE);
    digitalWrite(LED_RIGHT_EYE,LOW);  
    digitalWrite(LED_LEFT_EYE,LOW);  
    delay(LED_DELAY_VALUE);
    Serial.println("Testing the leds high then low");
  }

  if (TEST_RIGHT_PIRS)
  {
    int Right = digitalRead(PIR_RIGHT_EYE);
    Serial.print("Right Eye detects:");
    Serial.println(Right);
  }

  if (TEST_LEFT_PIRS)
  {
    int Left = digitalRead(PIR_LEFT_EYE);
    Serial.print("Left Eye detects:");
    Serial.println(Left);
  }
}


void setServoSignal(unsigned int servoIndex, unsigned int sig)
{
  if (sig < startPositions[servoIndex])
  {
    sig = startPositions[servoIndex];
  }
  else if (sig > maxPositions[servoIndex])
  {
    sig = maxPositions[servoIndex];
  }

  myservo[servoIndex].write(sig);
}

void setup() 
{  
  Serial.begin(BAUDRATE);  
  Serial.println("--- Ser Mon ---");
  Serial.print("Version:");
  Serial.println(version);
  setupPins();
}

void printHelp()
{
  Serial.println("--- Help ---");
  Serial.println("1. Find Person");
  Serial.println("2. Reset ");
  Serial.println("3. Test LEDs");
  Serial.println("4. Test Left Eye");
  Serial.println("5. Test Right Eye");
  Serial.println("6. Test Servos");
  Serial.println("7. Stop Test");
  Serial.println("+. Increase LED delay");
  Serial.println("-. Decrease LED delay");
  Serial.println("i. Information");

  Serial.println("h. Help Menu");
}

void information()
{
  Serial.print("LED_DELAY_VALUE = ");
  Serial.println(LED_DELAY_VALUE);
  
  if (currentState == idle)
    Serial.println("idle state");
  else if (currentState == test)
    Serial.println("test state");
  else
    Serial.println("search state");

  Serial.print("TEST_SERVOS = ");
  Serial.println(TEST_SERVOS);

  Serial.print("TEST_LEDS =");
  Serial.println(TEST_LEDS);
  
  Serial.print("TEST_LEFT_PIRS = ");
  Serial.println(TEST_LEFT_PIRS);

  Serial.print("TEST_RIGHT_PIRS = ");
  Serial.println(TEST_RIGHT_PIRS);

  Serial.print("eyeElevationL = ");
  Serial.println(eyeElevationL);
  
  Serial.print("eyeElevationR = ");
  Serial.println(eyeElevationR);

  Serial.print("eyeRotationL = ");
  Serial.println(eyeRotationL);
  
  Serial.print("eyeRotationR = ");
  Serial.println(eyeRotationL);

  Serial.println(version);
}

void setSearch()
{
  moveLeftEye = true;
  moveRightEye = true;
  changeState(search);
}

void loop() 
{    
  if (Serial.available())
  {
    char ch = Serial.read();
    Serial.print("lloop");
    if (ch == 'h')
    {
      printHelp();
    }
    else if (ch == '1')
    {
      setSearch();
    }
    else if (ch == '2')
    {
      reset();
    }
    else if (ch == '3')
    {
     changeState(test); 
     TEST_LEDS = true;
    }
    else if (ch == '4')
    {
     changeState(test);
     TEST_LEFT_PIRS = true; 
    }
    else if (ch == '5')
    {
      TEST_RIGHT_PIRS = true;
      changeState(test);
    }
    else if (ch == '6')
    { 
      resetTest();
      changeState(test);
      TEST_SERVOS = true;
    }
    else if (ch == '7')    
    {
      changeState(idle); 
      TEST_LEDS = false;
      TEST_SERVOS = false;
      TEST_LEFT_PIRS = false;
      TEST_RIGHT_PIRS = false;
    }
    else if (ch == 'i')
    {
      information(); 
    }
    else if (ch == '+')
    {
      LED_DELAY_VALUE = LED_DELAY_VALUE  + LED_INCREMENT;
    }
    else if (ch == '-')
    {
      if (LED_DELAY_VALUE > LED_INCREMENT)
        LED_DELAY_VALUE = LED_DELAY_VALUE  - LED_INCREMENT;
    }
    else 
    {
      Serial.println("Unknown command use h for help");
    } 
  }
  
  if (currentState == search)
    scanArea(); 
  else if (currentState == test)
    testSystem();
}
